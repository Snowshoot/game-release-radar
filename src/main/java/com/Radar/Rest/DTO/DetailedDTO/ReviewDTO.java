package com.Radar.Rest.DTO.DetailedDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class ReviewDTO {
    private Integer stars;
    private String review;
    private Integer gameId;
}
