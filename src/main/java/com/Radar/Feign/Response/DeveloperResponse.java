package com.Radar.Feign.Response;

import lombok.*;


@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class DeveloperResponse {
    @NonNull private Integer id;
    @NonNull private String name;
}
