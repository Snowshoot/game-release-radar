package com.Radar.Service.DbService;

import com.Radar.Entity.DeveloperEntity;
import com.Radar.Entity.GameEntity;
import com.Radar.Feign.Response.DeveloperResponse;
import com.Radar.Repository.DeveloperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DeveloperService {
    @Autowired
    private DeveloperRepository developerRepository;

    public List<DeveloperEntity> findAll(){return developerRepository.findAll();}

    public Optional<DeveloperEntity> findById(Integer developerID){return developerRepository.findById(developerID);}

    public DeveloperEntity findByApiId(Integer apiId){
        return developerRepository.findByApiId(apiId);
    }

    public void save(DeveloperEntity developer){
        developerRepository.save(developer);
    }

    public void update(DeveloperResponse developer){
        DeveloperEntity tempDev = developerRepository.findByApiId(developer.getId());
        tempDev.setName(developer.getName());
        developerRepository.save(tempDev);
    }
}