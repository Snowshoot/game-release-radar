package com.Radar.Repository;

import com.Radar.Entity.DeveloperEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface DeveloperRepository extends JpaRepository<DeveloperEntity, Integer> {

    @Query("SELECT d from DeveloperEntity d where d.developerApiId = :apiId")
    DeveloperEntity findByApiId(@Param("apiId") Integer apiId);
}
