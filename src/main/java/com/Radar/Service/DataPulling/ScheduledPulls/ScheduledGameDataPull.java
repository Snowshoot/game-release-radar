package com.Radar.Service.DataPulling.ScheduledPulls;

import com.Radar.Feign.Response.GameResponse;
import com.Radar.Service.DataPulling.CompareAndSaveOrUpdate.GameSaveOrUpdate.PulledGamesSaveOrUpdate;
import com.Radar.Service.DataPulling.DataPullers.GameDataPuller;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class ScheduledGameDataPull {
    @Autowired
    private GameDataPuller gameDataPuller;
    @Autowired
    private PulledGamesSaveOrUpdate pulledGamesSaveOrUpdate;

    @Scheduled(fixedDelay = 86400000)
    public void gamePullCheckAndDetermineSaveOrUpdate(){
        List<GameResponse> games = (gameDataPuller.getGames());
        games.forEach(gameResponse -> pulledGamesSaveOrUpdate.compareGameAndSaveOrUpdate(gameResponse));
    }
}

