package com.Radar.Service.DataPulling.DataPullers;

import com.Radar.Feign.Clients.GenreClient;
import com.Radar.Feign.Response.GenreResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreDataPuller {
    @Autowired
    private GenreClient genreClient;
    public List<GenreResponse> getGenres(){
        return genreClient.getGenres(1,20);
    }
}
