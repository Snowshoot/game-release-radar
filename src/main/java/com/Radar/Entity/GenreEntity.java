package com.Radar.Entity;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@Table(name = "genres", uniqueConstraints = {
        @UniqueConstraint(columnNames = "genre")})
public class GenreEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "genre_id", unique = true, nullable = false)
    private Integer genreId;

    @Column(name = "genre", unique = true, nullable = false)
    @NonNull private String genre;

    @Column(name = "genre_api_id", unique = true, nullable = false)
    @NonNull private Integer genreApiId;

    @ManyToMany(mappedBy = "genres")
    @NonNull private Set<GameEntity> genreGames = new HashSet<>();
}
