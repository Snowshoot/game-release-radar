package com.Radar.Rest.DTO.SimpleDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class SimpleStoreDTO {
    private Integer id;
    private String name;
    private String logo;
}
