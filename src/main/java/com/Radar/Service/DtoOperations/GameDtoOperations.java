package com.Radar.Service.DtoOperations;

import com.Radar.Entity.GameEntity;
import com.Radar.Entity.ReviewEntity;
import com.Radar.Rest.DTO.DetailedDTO.GameDTO;
import com.Radar.Rest.DTO.DetailedDTO.StoreUrlDTO;
import com.Radar.Rest.DTO.SimpleDTO.*;
import com.Radar.Service.DbService.FollowedService;
import com.Radar.Service.DbService.GameService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
public class GameDtoOperations {
    @Autowired
    private GameService gameService;
    @Autowired
    private PublisherDtoOperations publisher2DTO;
    @Autowired
    private DeveloperDtoOperations developer2DTO;
    @Autowired
    private GenreDtoOperations genre2DTO;
    @Autowired
    private StoreUrlDtoOperations storeUrl2DTO;
    @Autowired
    private UserDtoOperations userDtoOperations;
    @Autowired
    private FollowedService followedService;

    public GamesWithCountDTO searchGamesPaged(Integer page, Integer pageSize, String query, String genres, String stores, Boolean noStores, Boolean tba, Boolean excludeReleased){
        ImmutablePair<List<GameEntity>, Long> games = gameService.gamesPaged(page, pageSize, query, genres, stores, noStores, tba, excludeReleased);
        return new GamesWithCountDTO(games.right, gamesToBasicDataGames(games.left));
    }

    public List<SimpleGameDTO> gamesToBasicDataGames(List<GameEntity> games){
        return games.stream().distinct().map(game -> new SimpleGameDTO(game.getGameId(), game.getTitle(), game.getCover(), dateFormatUtil(game.getRelease()), game.getAverageRating(), game.getTba())).collect(Collectors.toList());
    }

    public List<SimpleGameDTO> userFollowed(){
        List<GameEntity> games = followedService.userFollowed(userDtoOperations.getCurrentUser());
        return games.stream().map(game -> new SimpleGameDTO(game.getGameId(), game.getTitle(), game.getCover(), dateFormatUtil(game.getRelease()), game.getAverageRating(), game.getTba())).collect(Collectors.toList());
    }

    @Transactional
    public GameDTO gameToGameDetails(Integer gameId){
        Optional<GameEntity> game = gameService.findById(gameId);
        List<SimpleGenreDTO> genres = genre2DTO.gameGenreSetToNames(game.get().getGenres());
        List<SimplePublisherDTO> publishers = publisher2DTO.gamePublisherSetToNames(game.get().getPublishers());
        List<SimpleDeveloperDTO> developers = developer2DTO.gameDeveloperSetToNames(game.get().getDevelopers());
        List<StoreUrlDTO> stores = storeUrl2DTO.gameStoresToUrls(game.get().getStores());
        return new GameDTO(game.get().getGameId(), game.get().getTitle(), game.get().getCover(), dateFormatUtil(game.get().getRelease()), game.get().getDescription(), game.get().getAverageRating(), game.get().getTba(), genres, stores, developers, publishers);
    }

    private String dateFormatUtil(LocalDate preFormatDate){
        if(preFormatDate != null){
                return DateTimeFormatter.ofPattern("dd.MM.yyyy").format(preFormatDate);
        }
        else return null;
    }
}