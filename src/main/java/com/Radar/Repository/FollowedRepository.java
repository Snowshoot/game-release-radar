package com.Radar.Repository;

import com.Radar.Entity.FollowedEntity;
import com.Radar.Entity.GameEntity;
import com.Radar.Entity.GenreEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface FollowedRepository extends JpaRepository<FollowedEntity, Integer> {
    @Query("select f.gameId from FollowedEntity f where f.userId.login =:userName")
    List<GameEntity> findUserFollowed(@Param("userName") String userName);

    @Query("select f from FollowedEntity f where f.gameId.gameId = :gameId")
    List<FollowedEntity> findByGame(@Param("gameId") Integer gameId);

    @Query("select f from FollowedEntity f where f.userId.login = :login AND f.gameId.gameId = :gameId")
    FollowedEntity findByUserAndGame(@Param("login") String login, @Param("gameId") Integer gameId);

    @Query("select f from FollowedEntity f where f.notify = true AND f.userId.login = :login")
    List<FollowedEntity> findUserNotifications(@Param("login") String login);

    @Transactional
    @Modifying
    @Query("delete from FollowedEntity f where f.followedId = :followedId")
    Integer deleteFollowed(@Param("followedId") Integer followedId);
}
