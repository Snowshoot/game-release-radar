package com.Radar.Service.DtoOperations;

import com.Radar.Entity.GenreEntity;
import com.Radar.Rest.DTO.DetailedDTO.GenreDTO;
import com.Radar.Rest.DTO.SimpleDTO.SimpleGameDTO;
import com.Radar.Rest.DTO.SimpleDTO.SimpleGenreDTO;
import com.Radar.Service.DbService.GameService;
import com.Radar.Service.DbService.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class GenreDtoOperations {
    @Autowired
    private GenreService genreService;

    public List<SimpleGenreDTO> genresToNameList(){
        List<GenreEntity> genres = genreService.findAll();
        return genres.stream().distinct().map(genre -> new SimpleGenreDTO(genre.getGenreId(), genre.getGenre())).collect(Collectors.toList());
    }

    public List<SimpleGenreDTO> gameGenreSetToNames(Set<GenreEntity> genres){
        return genres.stream().distinct().map(genre -> new SimpleGenreDTO(genre.getGenreId(), genre.getGenre())).collect(Collectors.toList());
    }
}