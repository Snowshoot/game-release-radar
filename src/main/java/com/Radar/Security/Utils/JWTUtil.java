package com.Radar.Security.Utils;

import com.Radar.Service.DbService.TokenBlacklistService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JWTUtil {
    @Autowired
    private TokenBlacklistService tokenBlacklistService;

    @Value("${jwtsecretkey}")
    private String jwtKey;

    public String extractUserName(String token){
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpirationDate(String token){
        return extractClaim(token, Claims::getExpiration);
    }

    public String generateToken(UserDetails user){
        Map<String, Object> claims = new HashMap<>();
        return createToken(claims, user.getUsername());
    }

    public String generateRefresh(UserDetails user){
        Map<String, Object> claims = new HashMap<>();
        return createRefreshToken(claims, user.getUsername());
    }

    public Boolean validateToken(String token, UserDetails user){
        final String userName = extractUserName(token);
        return (userName.equals(user.getUsername()) && !isTokenBlacklisted(token));
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver){
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token){
        return Jwts.parser().setSigningKey(jwtKey).parseClaimsJws(token).getBody();
    }

    private String createRefreshToken(Map<String, Object> claims, String subject) {
        Date currentDate = new Date(System.currentTimeMillis());
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(currentDate)
                .setExpiration(new Date(currentDate.getTime() + 604800000))
                .signWith(SignatureAlgorithm.HS256, jwtKey).compact();
    }

    private String createToken(Map<String, Object> claims, String subject){
        Date currentDate = new Date(System.currentTimeMillis());
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(currentDate)
                .setExpiration(new Date(currentDate.getTime() + 86400000))
                .signWith(SignatureAlgorithm.HS256, jwtKey).compact();
    }

    private Boolean isTokenBlacklisted(String token){
       return tokenBlacklistService.findByToken(token) != null;
    }
}

