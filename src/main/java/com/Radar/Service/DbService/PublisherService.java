package com.Radar.Service.DbService;

import com.Radar.Entity.PublisherEntity;
import com.Radar.Feign.Response.PublisherResponse;
import com.Radar.Repository.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class PublisherService {
    @Autowired
    private PublisherRepository publisherRepository;

    public List<PublisherEntity> findAll(){
        return publisherRepository.findAll();
    }

    @Transactional
    public Optional<PublisherEntity> findById(Integer publisherId){
        return publisherRepository.findById(publisherId);
    }

    public PublisherEntity findByApiId(Integer apiId){
            return publisherRepository.findByApiId(apiId);
        }

    public void save(PublisherEntity publisher){
        publisherRepository.save(publisher);
    }

    public void update(PublisherResponse publisher){
        PublisherEntity tempPub = publisherRepository.findByApiId(publisher.getId());
        tempPub.setName(publisher.getName());
        publisherRepository.save(tempPub);
    }
}
