//package com.Radar.Service.DataPulling.ScheduledPulls;
//
//import com.Radar.Feign.Response.GenreResponse;
//import com.Radar.Service.DataPulling.CompareAndSaveOrUpdate.PulledGenreSaveOrUpdate;
//import com.Radar.Service.DataPulling.DataPullers.GenreDataPuller;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//import java.util.List;
//
//@Component
//@Slf4j
//public class ScheduledGenreDataPull {
//    @Autowired
//    private GenreDataPuller genreDataPuller;
//    @Autowired
//    private PulledGenreSaveOrUpdate pulledGenreSaveOrUpdate;
//
//    @Scheduled(fixedDelay = 86400000)
//    public void genrePullCheckAndDetermineSaveOrUpdate() {
//        List<GenreResponse> genres = genreDataPuller.getGenres();
//        genres.forEach(genre -> pulledGenreSaveOrUpdate.compareGenreAndSaveOrUpdate(genre));
//    }
//}
//
