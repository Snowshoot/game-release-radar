package com.Radar.Rest.Controllers;

import com.Radar.Entity.Enum.RefreshOrAuthTokenEnum;
import com.Radar.Rest.DTO.SimpleDTO.UserAuthenticationDTO;
import com.Radar.Security.Utils.JWTUtil;
import com.Radar.Service.DtoOperations.TokenOperations;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@RestController
public class JwtRestController {
    @Autowired
    TokenOperations tokenOperations;
    @Autowired
    JWTUtil jwtUtil;

    @PostMapping("/auth")
    public void createAuthenticationToken(@RequestBody UserAuthenticationDTO user, HttpServletResponse response) throws Exception{
        tokenOperations.authenticate(user, response);
    }

    @PostMapping("/blacklist")
    public void blacklistToken(@CookieValue("accessToken") String token, @CookieValue("refreshToken") String refreshToken, HttpServletResponse response){
        response.addCookie(tokenOperations.blacklistToken(refreshToken, RefreshOrAuthTokenEnum.REFRESH));
        response.addCookie(tokenOperations.blacklistToken(token, RefreshOrAuthTokenEnum.AUTHORIZATION));
    }
}


