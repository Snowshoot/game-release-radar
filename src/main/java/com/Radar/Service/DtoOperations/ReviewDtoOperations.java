package com.Radar.Service.DtoOperations;

import com.Radar.Entity.GameEntity;
import com.Radar.Entity.ReviewEntity;
import com.Radar.Entity.ReviewEntityPrimaryKey;
import com.Radar.Entity.UserEntity;
import com.Radar.Rest.DTO.DetailedDTO.ReviewDTO;
import com.Radar.Rest.DTO.SimpleDTO.GameReviewDTO;
import com.Radar.Rest.DTO.SimpleDTO.GameReviewsWithCount;
import com.Radar.Rest.DTO.SimpleDTO.UserPageReviewDTO;
import com.Radar.Rest.DTO.SimpleDTO.UserReviewsWithCount;
import com.Radar.Service.DbService.GameService;
import com.Radar.Service.DbService.ReviewService;
import com.Radar.Service.DbService.UserService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ReviewDtoOperations {
    @Autowired
    private ReviewService reviewService;
    @Autowired
    private GameService gameService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserDtoOperations userDtoOperations;

    public UserReviewsWithCount reviewsToDTOByUser(Integer page, Integer pageSize){
        Integer userId = userService.findByLogin(userDtoOperations.getCurrentUser()).getUserId();
        ImmutablePair<List<ReviewEntity>, Long> reviewsWithCount = reviewService.reviewsByUserPaged(userId, page, pageSize);
        List<ReviewEntity> reviews = reviewsWithCount.left;
        List<UserPageReviewDTO> reviewDTOS = reviews.stream().map(review -> new UserPageReviewDTO(review.getGameId().getGameId(),review.getGameId().getCover(), review.getStars(), review.getReview(), review.getGameId().getTitle())).collect(Collectors.toList());
        return new UserReviewsWithCount(reviewsWithCount.right, reviewDTOS);
    }

    public GameReviewsWithCount reviewsToDTOByGame(Integer gameId, Integer page, Integer pageSize){
        ImmutablePair<List<ReviewEntity>, Long> reviewsWithCount = reviewService.reviewsByGamePaged(gameId, page, pageSize);
        List<ReviewEntity> reviews = reviewsWithCount.left;
        List<GameReviewDTO> reviewDTOS = reviews.stream().map(review -> new GameReviewDTO(review.getStars(), review.getReview(), review.getUserId().getLogin())).collect(Collectors.toList());
        return new GameReviewsWithCount(reviewsWithCount.right, reviewDTOS);
    }

    public void saveNewReview(ReviewDTO reviewDTO){
        UserEntity currentUser = userService.findByLogin(userDtoOperations.getCurrentUser());
        ReviewEntityPrimaryKey reviewPK = new ReviewEntityPrimaryKey(reviewDTO.getGameId(), currentUser.getUserId());
        Optional<GameEntity> reviewGame = gameService.findById(reviewDTO.getGameId());
        ReviewEntity newReview = new ReviewEntity(reviewPK, reviewGame.get(), currentUser, reviewDTO.getStars(), reviewDTO.getReview());
        reviewService.save(newReview);
        gameService.updateAvgRating(reviewGame.get(), reviewService.reviewsByGame(reviewDTO.getGameId()));
    }

    public void removeReview(Integer gameId){
        UserEntity currentUser = userService.findByLogin(userDtoOperations.getCurrentUser());
        Integer userId = currentUser.getUserId();
        reviewService.delete(userId, gameId);
        gameService.updateAvgRating(gameService.findById(gameId).get(), reviewService.reviewsByGame(gameId));
    }
}
