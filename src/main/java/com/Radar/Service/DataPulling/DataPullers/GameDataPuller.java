package com.Radar.Service.DataPulling.DataPullers;

import com.Radar.Feign.Clients.GameClient;
import com.Radar.Feign.Response.GameResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GameDataPuller {
    @Autowired
    private GameClient gameClient;

    public List<GameResponse> getGames(){
        int apiLimitPageSize = 40;
        int count = (gameClient.getGamesCount()/apiLimitPageSize) + 1;
        List<GameResponse> allGames = new ArrayList<>();
        for(int i = 1; i <= count; i++){
            allGames.addAll(gameClient.getGames(i, apiLimitPageSize));
        }
        return allGames;
    }
}
