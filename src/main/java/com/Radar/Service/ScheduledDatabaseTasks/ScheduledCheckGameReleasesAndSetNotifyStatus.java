package com.Radar.Service.ScheduledDatabaseTasks;

import com.Radar.Entity.GameEntity;
import com.Radar.Service.DbService.FollowedService;
import com.Radar.Service.DbService.GameService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
@Slf4j
public class ScheduledCheckGameReleasesAndSetNotifyStatus {
    @Autowired
    private GameService gameService;
    @Autowired
    private FollowedService followedService;

    @Scheduled(fixedDelay = 86400000)
    private void findReleasingGames(){
        LocalDate tomorrow = LocalDate.now().plusDays(1);
        List<GameEntity> releasingGames = gameService.findReleasingInOneDay(tomorrow);
        releasingGames.forEach(game -> followedService.setNotificationForReleasingGames(game.getGameId()));
    }
}
