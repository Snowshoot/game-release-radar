package com.Radar.Rest.Controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomePageController {
    @GetMapping("/")  //Spring security throws exception on /
    public String homePage(){
        String home = "<h3>" + "Henlo Werdl" + "</h3>";
        return home;
    }
}
