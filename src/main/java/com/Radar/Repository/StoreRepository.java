package com.Radar.Repository;

import com.Radar.Entity.StoreEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StoreRepository extends JpaRepository<StoreEntity, Integer> {
    @Query("SELECT s from StoreEntity s, GameEntity g where g.gameApiId = :gameApiId")
    List<StoreEntity> findByGameId(@Param("gameApiId") Integer gameApiId);

    @Query("SELECT s from StoreEntity s where s.storeApiId = :apiId")
    StoreEntity findByApiId(@Param("apiId") Integer apiId);
}
