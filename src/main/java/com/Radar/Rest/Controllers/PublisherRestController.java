package com.Radar.Rest.Controllers;

import com.Radar.Rest.DTO.DetailedDTO.PublisherDTO;
import com.Radar.Rest.DTO.SimpleDTO.SimplePublisherDTO;
import com.Radar.Service.DtoOperations.PublisherDtoOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PublisherRestController {
    @Autowired
    private PublisherDtoOperations publisherDTOOperations;

    @GetMapping("/publishers")
    List<SimplePublisherDTO> findAll(){
        return publisherDTOOperations.publishersToNamesList();
    }

    @GetMapping("/publishers/{id}")
    PublisherDTO findById(@PathVariable(name = "id") Integer publisherId, @RequestParam(name = "page") Integer page, @RequestParam(name = "pageSize") Integer pageSize){
        return publisherDTOOperations.publisherToDetails(publisherId, page, pageSize);
    }
}
