package com.Radar.Rest.DTO.SimpleDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class NewPasswordDTO {
    private String oldPassword;
    private String newPassword;
}
