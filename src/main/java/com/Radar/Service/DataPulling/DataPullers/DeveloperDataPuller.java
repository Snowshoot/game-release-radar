package com.Radar.Service.DataPulling.DataPullers;

import com.Radar.Feign.Clients.DeveloperClient;
import com.Radar.Feign.Response.DeveloperResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeveloperDataPuller {
    @Autowired
    private DeveloperClient developerClient;
    public List<DeveloperResponse> getDevelopers(){
        return developerClient.getDevelopers(1,20);
    }
}
