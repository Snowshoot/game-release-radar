package com.Radar.Feign.Response;

import lombok.*;

@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class StoreResponse {
    private Integer id;
    private String name;
}
