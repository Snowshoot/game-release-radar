//package com.Radar.Service.DataPulling.ScheduledPulls;
//
//import com.Radar.Feign.Response.DeveloperResponse;
//import com.Radar.Service.DataPulling.DataPullers.DeveloperDataPuller;
//import com.Radar.Service.DataPulling.CompareAndSaveOrUpdate.PulledDevelopersSaveOrUpdate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import java.util.List;
//
//
//@Component
//public class ScheduledDevDataPull {
//    @Autowired
//    private DeveloperDataPuller devDataPuller;
//    @Autowired
//    private PulledDevelopersSaveOrUpdate pulledDevelopersSaveOrUpdate;
//
//    @Scheduled(fixedDelay = 86400000)
//    public void developerPullCheckAndDetermineSaveOrUpdate(){
//        List<DeveloperResponse> developers = devDataPuller.getDevelopers();
//        developers.forEach(developerResponse -> pulledDevelopersSaveOrUpdate.compareDeveloperAndSaveOrUpdate(developerResponse));
//    }
//}
