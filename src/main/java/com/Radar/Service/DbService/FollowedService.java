package com.Radar.Service.DbService;

import com.Radar.Entity.FollowedEntity;
import com.Radar.Entity.GameEntity;
import com.Radar.Entity.UserEntity;
import com.Radar.Repository.FollowedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FollowedService {
    @Autowired
    FollowedRepository followedRepository;
    public void addFollowed(UserEntity user, GameEntity followedGame){
        if(followedRepository.findByUserAndGame(user.getLogin(), followedGame.getGameId()) == null){
            followedRepository.save(new FollowedEntity(followedGame, user, false));
        }
    }
    public void removeFollowed(String userName, GameEntity unfollowedGame){
        FollowedEntity followedEntity = followedRepository.findByUserAndGame(userName, unfollowedGame.getGameId());
        if(followedEntity != null) {
            followedRepository.deleteFollowed(followedEntity.getFollowedId());
        }
    }

    public void setNotificationForReleasingGames(Integer gameId){
        List<FollowedEntity> followedList = followedRepository.findByGame(gameId);
        followedList.forEach(follow -> {
            follow.setNotify(true);
            followedRepository.save(follow);
        });
    }

    public List<GameEntity> findUserNotificationGames(String userName){
        List<FollowedEntity> followedList = followedRepository.findUserNotifications(userName);
        return followedList.stream().map(follow -> follow.getGameId()).collect(Collectors.toList());
    }

    public void disableNotifications(String userName){
        List<FollowedEntity> followedList = followedRepository.findUserNotifications(userName);
        followedList.forEach(follow -> {
            follow.setNotify(false);
            followedRepository.save(follow);
        });
    }

    public List<GameEntity> userFollowed(String userName){
        return followedRepository.findUserFollowed(userName);
    }
}
