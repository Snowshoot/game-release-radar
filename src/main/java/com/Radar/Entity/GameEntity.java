package com.Radar.Entity;

import lombok.*;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@Table(name = "games", uniqueConstraints = {
        @UniqueConstraint(columnNames = "title")
})
public class GameEntity {
    public GameEntity(String title, String cover, LocalDate release, String description, Integer gameApiId, Boolean tba, Set<GenreEntity> genres, Set<DeveloperEntity> developers, Set<PublisherEntity> publishers){
        this.title = title;
        this.cover = cover;
        this.release = release;
        this.description = description;
        this.gameApiId = gameApiId;
        this.tba = tba;
        this.genres = genres;
        this.developers = developers;
        this.publishers = publishers;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "game_id", unique = true, nullable = false)
    private Integer gameId;

    @Column(name = "title", unique = true, nullable = false)
    @NonNull private String title;

    @Column(name = "cover")
    @NonNull private String cover;

    @Column(name = "release")
    private LocalDate release;

    @Column(name= "description", length = 10000)
    private String description;

    @Column(name = "average_rating")
    private Double averageRating;

    @Column(name = "game_api_id", nullable = false)
    @NonNull private Integer gameApiId;

    @Column(name = "tba", nullable = false)
    @NonNull private Boolean tba;

    @OneToMany(mappedBy = "gameId")
    private Set<ReviewEntity> reviews = new HashSet<>();

    @OneToMany(mappedBy = "gameApiId")
    @NonNull private Set<StoreUrlEntity> stores = new HashSet<>();

    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(
            name= "games_developers",
            joinColumns = {@JoinColumn(name = "game_id")},
            inverseJoinColumns = {@JoinColumn(name = "developer_id")}
    )
    private Set<DeveloperEntity> developers = new HashSet<>();

    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(
            name= "games_publishers",
            joinColumns = {@JoinColumn(name = "game_id")},
            inverseJoinColumns = {@JoinColumn(name = "publisher_id")}
    )
    private Set<PublisherEntity> publishers = new HashSet<>();

    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(
            name= "games_genres",
            joinColumns = {@JoinColumn(name = "game_id")},
            inverseJoinColumns = {@JoinColumn(name = "genre_id")}
    )
    private Set<GenreEntity> genres = new HashSet<>();

    @OneToMany(mappedBy = "gameId")
    private Set<FollowedEntity> followed = new HashSet<>();
}