package com.Radar.Feign.Clients;

import com.Radar.Feign.Response.GenreResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@FeignClient(name = "receivedGenres", url = "localhost:8290")
public interface GenreClient {
    @RequestMapping(method = RequestMethod.GET, value = "/genres")
    List<GenreResponse> getGenres(@RequestParam Integer page, @RequestParam(name = "page_size") Integer pageSize);
}