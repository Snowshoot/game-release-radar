package com.Radar.Service.DataPulling.DataPullers;

import com.Radar.Feign.Clients.PublisherClient;
import com.Radar.Feign.Response.PublisherResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PublisherDataPuller {
    @Autowired
    private PublisherClient publisherClient;
    public List<PublisherResponse> getPublishers(){
        return publisherClient.getPublishers(1,20);
    }
}
