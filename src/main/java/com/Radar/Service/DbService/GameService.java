package com.Radar.Service.DbService;

import com.Radar.Entity.*;
import com.Radar.Feign.Response.GameResponse;
import com.Radar.Repository.GameRepostiory;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.JoinType;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class GameService {
    @Autowired
    private GameRepostiory gameRepostiory;

    public GameEntity findByApiId(Integer apiId){
        return gameRepostiory.findByApiId(apiId);
    }

    public Optional<GameEntity> findById(Integer gameId){return gameRepostiory.findById(gameId);}

    public List<GameEntity> findAll(){return gameRepostiory.findAll();}

    @Transactional
    public void save(GameEntity game){
        gameRepostiory.save(game);
    }

    public List<GameEntity> findReleasingSoon(Integer page, Integer pageSize){
        Pageable pageable = PageRequest.of(page - 1, pageSize);
        LocalDate currentDate = LocalDate.now();
        return gameRepostiory.findReleasingSoon(currentDate, pageable);
    }

    public List<GameEntity> findReleasingInOneDay(LocalDate dayAfterCurrent){
        return gameRepostiory.findReleasingInOneDay(dayAfterCurrent);
    }

    public List<GameEntity> findTopRated(Integer page, Integer pageSize){
        Pageable pageable = PageRequest.of(page - 1, pageSize);
        return gameRepostiory.findTopRated(pageable);
    }

    public List<GameEntity> mostFollowed(Integer page, Integer pageSize){
        Pageable pageable = PageRequest.of(page - 1, pageSize);
        return gameRepostiory.findMostFollowed(pageable);
    }

    public ImmutablePair<List<GameEntity>, Long> gamesPaged(Integer page, Integer pageSize, String query, String genres, String stores, Boolean noStores, Boolean tba, Boolean excludeReleased){
        Pageable pageable = PageRequest.of(page - 1, pageSize);
        Specification<GameEntity> specification = Specification.where(null);
        if(excludeReleased != null && excludeReleased){
            specification = specification.and(excludeReleased());
        }
        if(tba != null){
            specification = specification.and(areNotTba(tba));
        }
        if(noStores != null){
            specification = specification.and(noStores(noStores));
        }
        if(query != null && query.length() > 0){
            specification = specification.and(titleContains(query));
        }
        if(genres != null && genres.length() > 0) {
            List<Integer> genreIds = Stream.of(genres.split(",")).map(String::trim).map(Integer::parseInt).collect(Collectors.toList());
            specification = specification.and(genresAre(genreIds));
        }
        if(stores != null && stores.length() > 0) {
            List<Integer> storeIds = Stream.of(stores.split(",")).map(String::trim).map(Integer::parseInt).collect(Collectors.toList());
            specification = specification.and(storesAre(storeIds));
        }
        return ImmutablePair.of(
                gameRepostiory.findAll(
                specification,
                pageable
                ).stream().collect(Collectors.toList()),
                gameRepostiory.count(specification));
    }

    public ImmutablePair<List<GameEntity>, Long> gamesPagedByDev(Integer devId, Integer page, Integer pageSize){
        Pageable pageable = PageRequest.of(page - 1, pageSize);
        Specification<GameEntity> spec = Specification.where(byDev(devId));
        return ImmutablePair.of(
                gameRepostiory.findAll(
                        spec,
                        pageable
                ).stream().collect(Collectors.toList()),
                gameRepostiory.count(spec));
    }

    public ImmutablePair<List<GameEntity>, Long> gamesPagedByPub(Integer pubId, Integer page, Integer pageSize){
        Pageable pageable = PageRequest.of(page - 1, pageSize);
        Specification<GameEntity> spec = Specification.where(byPub(pubId));
        return ImmutablePair.of(
                gameRepostiory.findAll(
                        spec,
                        pageable
                ).stream().collect(Collectors.toList()),
                gameRepostiory.count(spec));
    }

    public void updateAvgRating(GameEntity game, List<ReviewEntity> reviews){
        List<Integer> currentRating = reviews.stream().map(ReviewEntity::getStars).collect(Collectors.toList());
        int sum = currentRating.stream().mapToInt(Integer::intValue).sum();
        double avg = ((double) sum) / (currentRating.size());
        avg = Math.round(avg * 100.0) / 100.0;
        game.setAverageRating(avg);
        gameRepostiory.save(game);
    }

    @Transactional
    public void update(GameResponse game, Set<GenreEntity> gameGenres, Set<DeveloperEntity> gameDevs, Set<PublisherEntity> gamePubs, Set<StoreUrlEntity> gameStoreUrls){
        GameEntity tempGame = gameRepostiory.findByApiId(game.getId());
        tempGame.setTitle(game.getName());
        tempGame.setCover(game.getCover());
        tempGame.setDescription(game.getDescription());
        tempGame.setRelease(game.getReleased());
        tempGame.setTba(game.getTba());
        tempGame.setGenres(gameGenres);
        tempGame.setDevelopers(gameDevs);
        tempGame.setPublishers(gamePubs);
        tempGame.setStores(gameStoreUrls);
    }

    static Specification<GameEntity> titleContains(String title) {
        return (game, cq, cb) -> cb.like(cb.upper(game.get("title")) , "%" + title.toUpperCase() + "%");
    }

    static Specification<GameEntity> genresAre(List<Integer> genreIds) {
        return (game, cq, cb) -> {
            cq.distinct(true);
            return game.join("genres").get("genreId").in(genreIds);
        };
    }

    static Specification<GameEntity> storesAre(List<Integer> storeIds) {
        return (game, cq, cb) -> {
            cq.distinct(true);
            return game.join("stores").join("storeApiId").get("storeId").in(storeIds);
        };
    }

    static Specification<GameEntity> noStores(Boolean noStores) {
        return (game, cq, cb) -> {
            cq.distinct(true);
            if(noStores){return game.join("stores", JoinType.LEFT).isNull();}
            else{return game.join("stores").get("gameApiId").isNotNull();}
        };
    }

    static Specification<GameEntity> areNotTba(Boolean tba) {
        return (game, cq, cb) -> cb.equal(game.get("tba"),tba);
    }

    static Specification<GameEntity> byDev(Integer devId) {
        return (game, cq, cb) -> {
            cq.distinct(true);
            return cb.equal(game.join("developers").get("developerId"), devId);
        };
    }

    static Specification<GameEntity> excludeReleased(){
        return (game, cq, cb) -> cb.or(cb.greaterThan(game.get("release"), LocalDate.now()), cb.equal(game.get("tba"), true));
    }

    static Specification<GameEntity> byPub(Integer pubId) {
        return (game, cq, cb) -> {
            cq.distinct(true);
            return cb.equal(game.join("publishers").get("publisherId"), pubId);
        };
    }
}


