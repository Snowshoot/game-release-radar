package com.Radar.Entity;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@Table(name = "stores", uniqueConstraints = {
        @UniqueConstraint(columnNames = "name")
})
public class StoreEntity {
    public StoreEntity(String name, Integer storeApiId){
        this.name = name;
        this.storeApiId = storeApiId;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "store_id", unique = true, nullable = false)
    private Integer storeId;

    @Column(name = "name", unique = true, nullable = false)
    @NonNull private String name;

    @Column(name = "store_api_id", unique = true, nullable = false)
    @NonNull private Integer storeApiId;

    @Column(name = "logo", unique = true)
    @NonNull private String logo;

    @OneToMany(mappedBy = "storeApiId")
    private Set<StoreUrlEntity> games = new HashSet<>();
}
