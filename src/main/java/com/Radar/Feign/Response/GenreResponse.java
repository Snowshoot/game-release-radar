package com.Radar.Feign.Response;

import lombok.*;

@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class GenreResponse {
    private Integer id;
    private String name;
}
