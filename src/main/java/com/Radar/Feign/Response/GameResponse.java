package com.Radar.Feign.Response;

import lombok.*;

import java.time.LocalDate;
import java.util.Set;

@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class GameResponse {
    public GameResponse(Integer id, String name, String cover, LocalDate released, String description, Boolean tba){
        this.id = id;
        this.name = name;
        this.cover = cover;
        this.released = released;
        this.description = description;
        this.tba = tba;
    }
    private Integer id;
    private String name;
    private String cover;
    private LocalDate released;
    private String description;
    private Boolean tba;
    @EqualsAndHashCode.Exclude private Set<GenreResponse> genres;
    @EqualsAndHashCode.Exclude private Set<StoreUrlResponse> stores;
    @EqualsAndHashCode.Exclude private Set<DeveloperResponse> developers;
    @EqualsAndHashCode.Exclude private Set<PublisherResponse> publishers;
}