package com.Radar.Entity;

import lombok.*;

import javax.management.relation.Role;
import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(columnNames = "login"),
        @UniqueConstraint(columnNames = "email")})
public class UserEntity implements Serializable {
    public UserEntity(String login, String password, String email, Set<RoleEntity> roles){
        this.login = login;
        this.password = password;
        this.email = email;
        this.roles = roles;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "user_id", unique = true, nullable = false)
    private Integer userId;

    @Column(name = "login", unique = true, nullable = false)
    @NonNull private String login;

    @Column(name = "password", nullable = false)
    @NonNull private String password;

    @Column(name = "email", unique = true, nullable = false)
    @NonNull private String email;

    @OneToMany(mappedBy = "userId")
    private Set<ReviewEntity> reviews = new HashSet<>();

    @ManyToMany(cascade = {CascadeType.MERGE})
    @JoinTable(
            name= "users_roles",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")}
    )
    private Set<RoleEntity> roles = new HashSet<>();

    @OneToMany(mappedBy = "userId")
    private Set<FollowedEntity> followed = new HashSet<>();

}
