package com.Radar.Config;

import com.Radar.Security.Filters.JWTRequestFilter;
import com.Radar.Service.DbService.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.bind.annotation.GetMapping;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserDetailsServiceImpl userDetailsService;
    @Autowired
    private JWTRequestFilter jwtRequestFilter;

    @Override
    public void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.csrf().disable();
        httpSecurity.cors();
        httpSecurity.authorizeRequests()
                .antMatchers("/users/notifications").permitAll()
                .antMatchers("/users/followed").authenticated()
                .antMatchers("/users/checker/*").permitAll()
                .regexMatchers("/games/\\d+").permitAll()
                .antMatchers("/games/{\\d+}/reviews").permitAll()
                .antMatchers("/games").permitAll()
                .antMatchers("/games/popular").permitAll()
                .antMatchers("/games/rating").permitAll()
                .antMatchers("/games/soon").permitAll()
                .antMatchers("/developers/**").permitAll()
                .antMatchers("/genres/**").permitAll()
                .antMatchers("/publishers/**").permitAll()
                .antMatchers("/reviews/user/*", "/reviews/game/*").permitAll()
                .antMatchers("/stores/**").permitAll()
                .antMatchers("/users", "/users/auth").permitAll()
                .antMatchers("/auth").permitAll()
                .antMatchers("/test").permitAll()
                .antMatchers("/users/role").hasRole("ADMIN")
                .anyRequest().authenticated();
        httpSecurity.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception{
        return super.authenticationManagerBean();
    }
}
