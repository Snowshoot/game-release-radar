package com.Radar.Entity;

import lombok.*;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "store_urls", uniqueConstraints = {
        @UniqueConstraint(columnNames = "url")
})
public class StoreUrlEntity {
    @EmbeddedId
    private StoreUrlEntityPrimaryKey urlId;

    @ManyToOne
    @MapsId("gameApiId")
    @JoinColumn(name = "game_api_id")
    private GameEntity gameApiId;

    @ManyToOne
    @MapsId("storeApiId")
    @JoinColumn(name = "store_api_id")
    private StoreEntity storeApiId;

    @Column(name = "store_url_api_id")
    private Integer storeUrlApiId;

    @Column(name = "url")
    private String url;
}