package com.Radar.Service.DataPulling.CompareAndSaveOrUpdate;

import com.Radar.Entity.PublisherEntity;

import com.Radar.Feign.Response.PublisherResponse;
import com.Radar.Service.DbService.PublisherService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PulledPublishersSaveOrUpdate {
    @Autowired
    private PublisherService publisherService;

    public void comparePublisherAndSaveOrUpdate(PublisherResponse newPub){
        PublisherEntity basePub = publisherService.findByApiId(newPub.getId());
        if(basePub != null) {
            PublisherResponse oldPub = transformPubEntityToResponse(basePub);
            if (!newPub.equals(oldPub)) {
                publisherService.update(newPub);
            }
            else {
                log.info("Publisher was not updated");
            }
        }
        else{
            PublisherEntity tempPublisher = new PublisherEntity(newPub.getName(), newPub.getId());
            publisherService.save(tempPublisher);
        }
    }

    private PublisherResponse transformPubEntityToResponse(PublisherEntity basePub) {
        return new PublisherResponse(basePub.getPublisherApiId(), basePub.getName());
    }
}