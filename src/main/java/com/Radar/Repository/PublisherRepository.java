package com.Radar.Repository;

import com.Radar.Entity.PublisherEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PublisherRepository extends JpaRepository<PublisherEntity, Integer> {
    @Query("SELECT p from PublisherEntity p WHERE p.publisherId = :publisherId")
    Optional<PublisherEntity> findById();

    @Query("SELECT p from PublisherEntity p where p.publisherApiId = :apiId")
    PublisherEntity findByApiId(@Param("apiId") Integer apiId);
}
