package com.Radar.Rest.DTO.DetailedDTO;

import com.Radar.Rest.DTO.SimpleDTO.SimpleGameDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class GenreDTO {
    private Integer id;
    private String genre;
    private List<SimpleGameDTO> basicGames;
}
