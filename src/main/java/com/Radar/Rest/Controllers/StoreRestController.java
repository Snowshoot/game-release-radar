package com.Radar.Rest.Controllers;

import com.Radar.Rest.DTO.SimpleDTO.SimpleStoreDTO;
import com.Radar.Rest.DTO.DetailedDTO.StoreDTO;
import com.Radar.Service.DtoOperations.StoreDtoOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StoreRestController {
    @Autowired
    private StoreDtoOperations storeDTOOperations;

    @GetMapping("/stores")
    List<SimpleStoreDTO> findAll() {
        return storeDTOOperations.storesToNameAndLogo();
    }
}
