package com.Radar.Service.DataPulling.CompareAndSaveOrUpdate;

import com.Radar.Entity.DeveloperEntity;
import com.Radar.Feign.Response.DeveloperResponse;

import com.Radar.Service.DbService.DeveloperService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PulledDevelopersSaveOrUpdate {
    @Autowired
    private DeveloperService developerService;

    public void compareDeveloperAndSaveOrUpdate(DeveloperResponse newDev){
        DeveloperEntity baseDev = developerService.findByApiId(newDev.getId());
            if(baseDev != null) {
                DeveloperResponse oldDev = transformDevEntityToResponse(baseDev);
                if (!newDev.equals(oldDev)) {
                    developerService.update(newDev);
                }
                else {
                    log.info("Developer was not updated");
                }
            }
            else{
                DeveloperEntity tempDeveloper = new DeveloperEntity(newDev.getName(), newDev.getId());
                developerService.save(tempDeveloper);
            }
    }
    private DeveloperResponse transformDevEntityToResponse(DeveloperEntity baseDev) {
        return new DeveloperResponse(baseDev.getDeveloperApiId(), baseDev.getName());
    }
}