package com.Radar.Rest.DTO.SimpleDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class NewEmailDTO {
    private String oldPassword;
    private String newEmail;
}
