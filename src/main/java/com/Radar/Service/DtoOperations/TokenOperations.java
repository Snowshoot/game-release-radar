package com.Radar.Service.DtoOperations;

import com.Radar.Entity.Enum.RefreshOrAuthTokenEnum;
import com.Radar.Entity.TokenBlacklistEntity;
import com.Radar.Rest.DTO.SimpleDTO.UserAuthenticationDTO;
import com.Radar.Security.Utils.JWTUtil;
import com.Radar.Service.DbService.TokenBlacklistService;
import com.Radar.Service.DbService.UserDetailsServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

@Component
@Slf4j
public class TokenOperations {
    @Autowired
    JWTUtil jwtUtil;
    @Autowired
    TokenBlacklistService tokenBlacklistService;
    @Autowired
    private UserDetailsServiceImpl userDetailsService;
    @Autowired
    private AuthenticationManager authenticationManager;

    public Cookie blacklistToken(String token, RefreshOrAuthTokenEnum type) {
        String cookieName;
        if (token != null) {
            Date expiration = jwtUtil.extractExpirationDate(token);
            TokenBlacklistEntity blacklistToken = new TokenBlacklistEntity(token, type, expiration);
            tokenBlacklistService.save(blacklistToken);
            if (type == RefreshOrAuthTokenEnum.AUTHORIZATION) {
                cookieName = "accessToken";
            } else {
                cookieName = "refreshToken";
            }
            Cookie logoutCookie = new Cookie(cookieName, null);
            logoutCookie.setHttpOnly(true);
            logoutCookie.setMaxAge(24 * 60 * 60);
            return logoutCookie;
        }
        return null;
    }

    public void authenticate(UserAuthenticationDTO user, HttpServletResponse response) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getLogin(), user.getPassword()));
        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password", e);
        }
        final UserDetails userDetails = userDetailsService.loadUserByUsername(user.getLogin());
        final String jwt = jwtUtil.generateToken(userDetails);
        final String refresh = jwtUtil.generateRefresh(userDetails);
        Cookie accessToken = new Cookie("accessToken", jwt);
        Cookie refreshToken = new Cookie("refreshToken", refresh);
        accessToken.setHttpOnly(true);
        accessToken.setMaxAge(7 * 24 * 60 * 60);
        refreshToken.setMaxAge(7 * 24 * 60 * 60);
        refreshToken.setHttpOnly(true);
        response.addCookie(accessToken);
        response.addCookie(refreshToken);
    }

    public void createAccessTokenOnValidRefresh(String oldRefresh, HttpServletResponse response) {
        if (oldRefresh != null) {
            final UserDetails userDetails = userDetailsService.loadUserByUsername(jwtUtil.extractUserName(oldRefresh));
            if(jwtUtil.validateToken(oldRefresh, userDetails)) {
                final String jwt = jwtUtil.generateToken(userDetails);
                TokenBlacklistEntity refreshBlacklist = new TokenBlacklistEntity(oldRefresh, RefreshOrAuthTokenEnum.REFRESH, jwtUtil.extractExpirationDate(oldRefresh));
                tokenBlacklistService.save(refreshBlacklist);
                final String newRefresh = jwtUtil.generateRefresh(userDetails);
                Cookie accessToken = new Cookie("accessToken", jwt);
                Cookie refreshToken = new Cookie("refreshToken", newRefresh);
                accessToken.setHttpOnly(true);
                accessToken.setMaxAge(7 * 24 * 60 * 60);
                accessToken.setPath("/");
                refreshToken.setMaxAge(7 * 24 * 60 * 60);
                refreshToken.setHttpOnly(true);
                refreshToken.setPath("/");
                response.addCookie(accessToken);
                response.addCookie(refreshToken);
            }
        }
    }
}

