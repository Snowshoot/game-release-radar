package com.Radar.Service.DataPulling.CompareAndSaveOrUpdate.GameSaveOrUpdate;

import com.Radar.Entity.GameEntity;
import com.Radar.Entity.StoreEntity;
import com.Radar.Entity.StoreUrlEntity;
import com.Radar.Entity.StoreUrlEntityPrimaryKey;
import com.Radar.Feign.Response.StoreUrlResponse;
import com.Radar.Service.DbService.GameService;
import com.Radar.Service.DbService.StoreService;
import com.Radar.Service.DbService.StoreUrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class GameStoreUrlUpdateUtil {
    @Autowired
    private StoreUrlService storeUrlService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private GameService gameService;

    public Set<StoreUrlEntity> storeUrlResponseToEntities(Set<StoreUrlResponse> storeUrlResponse, Integer gameApiId) {
        return storeUrlResponse.stream()
                .peek(store -> storeUrlSaveIfNotExists(store, gameApiId))
                .map(store -> storeUrlService.findByApiId(store.getStoreUrlApiId())).collect(Collectors.toSet());
    }

    private void storeUrlSaveIfNotExists(StoreUrlResponse storeUrlResponse, Integer gameApiId){
        if(storeUrlService.findByApiId(storeUrlResponse.getStoreUrlApiId()) == null){
            StoreUrlEntityPrimaryKey urlPk = new StoreUrlEntityPrimaryKey(gameApiId, storeUrlResponse.getStoreApiId());
            GameEntity tempGame = gameService.findByApiId(gameApiId);
            StoreEntity tempStore = storeService.findByApiId(storeUrlResponse.getStoreApiId());
            StoreUrlEntity tempStoreUrl = new StoreUrlEntity(urlPk, tempGame, tempStore, storeUrlResponse.getStoreUrlApiId(), storeUrlResponse.getUrl());
            storeUrlService.save(tempStoreUrl);
        }
    }

    public Boolean areNotSame(Set<StoreUrlResponse> newGameStores, Set<StoreUrlEntity> baseGameStores){
        Boolean areNotSame = !gameResponseToGameStoreUrls(newGameStores).equals(getGameStoreUrls(baseGameStores));
        if(areNotSame){
            newGameStores.forEach(store -> storeUrlService.update(store));
        }
        return areNotSame;
    }

    public List<String> getGameStoreUrls(Set<StoreUrlEntity> oldStoreUrls) {
        return oldStoreUrls.stream().map(StoreUrlEntity::getUrl).sorted().collect(Collectors.toList());
    }

    public List<String> gameResponseToGameStoreUrls(Set<StoreUrlResponse> storeUrlResponse) {
        return storeUrlResponse.stream().map(StoreUrlResponse::getUrl).sorted().collect(Collectors.toList());
    }
}
