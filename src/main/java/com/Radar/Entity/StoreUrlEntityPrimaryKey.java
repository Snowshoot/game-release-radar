package com.Radar.Entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Embeddable
public class StoreUrlEntityPrimaryKey implements Serializable {
    @Column(name = "game_api_id")
    private Integer gameApiId;

    @Column(name = "store_api_id")
    private Integer storeApiId;
}
