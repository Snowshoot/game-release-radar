package com.Radar.Rest.Controllers;

import com.Radar.Rest.DTO.DetailedDTO.DeveloperDTO;
import com.Radar.Rest.DTO.SimpleDTO.SimpleDeveloperDTO;
import com.Radar.Service.DtoOperations.DeveloperDtoOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DeveloperRestController {
    @Autowired
    private DeveloperDtoOperations developerDTOOperations;

    @GetMapping("/developers")
    List<SimpleDeveloperDTO> findAll(){
        return developerDTOOperations.developersToNamesList();
    }

    @GetMapping("/developers/{id}")
    DeveloperDTO findById(@PathVariable(name = "id") Integer developerId, @RequestParam(name = "page") Integer page, @RequestParam(name = "pageSize") Integer pageSize){
        return developerDTOOperations.developerToDeveloperDetails(developerId, page, pageSize);
    }
}
