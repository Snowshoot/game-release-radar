package com.Radar.Repository;

import com.Radar.Entity.GameEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface GameRepostiory extends JpaRepository<GameEntity, Integer>, JpaSpecificationExecutor<GameEntity> {
    @Query("SELECT g from GameEntity g where g.gameApiId = :apiId")
    GameEntity findByApiId(@Param("apiId") Integer apiId);

    @Query("select g from GameEntity g where g.averageRating is not null order by g.averageRating desc")
    List<GameEntity> findTopRated(Pageable pageable);

    @Query("select g, count(g.gameId) as counted from GameEntity g join g.followed group by g.gameId order by counted desc")
    List<GameEntity> findMostFollowed(Pageable pageable);

    @Query("Select g from GameEntity g WHERE g.release > :currentDate ORDER BY g.release")
    List<GameEntity> findReleasingSoon(@Param("currentDate") LocalDate currentDate, Pageable pageable);

    @Query("select g from GameEntity g WHERE g.release = :dayAfterCurrent")
    List<GameEntity> findReleasingInOneDay(@Param("dayAfterCurrent") LocalDate dayAfterCurrent);
}