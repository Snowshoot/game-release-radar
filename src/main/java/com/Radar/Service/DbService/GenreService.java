package com.Radar.Service.DbService;

import com.Radar.Entity.GenreEntity;
import com.Radar.Feign.Response.GenreResponse;
import com.Radar.Repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class GenreService {
    @Autowired
    private GenreRepository genreRepository;

    public List<GenreEntity> findAll() {return genreRepository.findAll();}

    public Optional<GenreEntity> findById(Integer genreId){return genreRepository.findById(genreId);}

    public void save(GenreEntity genre){
        genreRepository.save(genre);
    }

    public List<GenreEntity> findByGameId(Integer apiId){
        return genreRepository.findByGameId(apiId);
    }

    public GenreEntity findByApiId(Integer apiId){
        return genreRepository.findByApiId(apiId);
    }

    public void update(GenreResponse genre){
        GenreEntity tempGenre = genreRepository.findByApiId(genre.getId());
        tempGenre.setGenre(genre.getName());
    }
}
