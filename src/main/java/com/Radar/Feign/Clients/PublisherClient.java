package com.Radar.Feign.Clients;

import com.Radar.Feign.Response.PublisherResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@FeignClient(name = "receivedPublishers", url = "localhost:8290")
public interface PublisherClient {
    @RequestMapping(method = RequestMethod.GET, value = "/publishers")
    List<PublisherResponse> getPublishers(@RequestParam Integer page, @RequestParam(name = "page_size") Integer pageSize);
}