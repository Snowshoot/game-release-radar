package com.Radar.Feign.Clients;

import com.Radar.Feign.Response.GameResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "receivedGames", url = "localhost:8290")
public interface GameClient {
    @RequestMapping(method = RequestMethod.GET, value = "/games")
    List<GameResponse> getGames(@RequestParam Integer page, @RequestParam(name = "page_size") Integer pageSize);

    @RequestMapping(method = RequestMethod.GET, value = "/games/count")
    Integer getGamesCount();
}