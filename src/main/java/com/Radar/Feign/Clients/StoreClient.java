package com.Radar.Feign.Clients;

import com.Radar.Feign.Response.StoreResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;

@FeignClient(name = "receivedStores", url = "localhost:8290")
public interface StoreClient {
    @RequestMapping(method = RequestMethod.GET, value = "/stores")
    ArrayList<StoreResponse> getStores(@RequestParam Integer page, @RequestParam(name = "page_size") Integer pageSize);
}