package com.Radar.Rest.Controllers;

import com.Radar.Rest.DTO.DetailedDTO.GameDTO;
import com.Radar.Rest.DTO.DetailedDTO.UserDTO;
import com.Radar.Rest.DTO.SimpleDTO.*;
import com.Radar.Service.DtoOperations.GameDtoOperations;
import com.Radar.Service.DtoOperations.NotificationOperations;
import com.Radar.Service.DtoOperations.ReviewDtoOperations;
import com.Radar.Service.DtoOperations.UserDtoOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.LockSupport;
import java.util.stream.Collectors;

@RestController
public class UserRestController {
    @Autowired
    private UserDtoOperations userDtoOperations;
    @Autowired
    private GameDtoOperations gameDtoOperations;
    @Autowired
    private ReviewDtoOperations reviewDtoOperations;
    @Autowired
    private NotificationOperations notificationOperations;

    @PostMapping("/users")
    public void saveNewUser(@RequestBody UserDTO user) {
        userDtoOperations.saveNewUser(user);
    }

    @PutMapping("/users/role")
    public void addRole(@RequestParam(name = "userName") String userName, @RequestParam(name = "roleName") Integer roleId) {
        userDtoOperations.addRole(userName, roleId);
    }

    @DeleteMapping("/users/role")
    public void removeRole(@RequestParam(name = "userName") String userName, @RequestParam(name = "roleName") Integer roleId) {
        userDtoOperations.removeRole(userName, roleId);
    }

    @GetMapping("users/reviews")
    public UserReviewsWithCount findByUser(@RequestParam(name = "page") Integer page, @RequestParam(name = "pageSize") Integer pageSize) {
        return reviewDtoOperations.reviewsToDTOByUser(page, pageSize);
    }

    @GetMapping("users/notifications")
    public GamesWithCountDTO getUserNotification() {
        return notificationOperations.countedNotifications();
    }

    @PutMapping("users/notifications")
    public void removeUserNotifications() {
        notificationOperations.removeNotifications();
    }

    @GetMapping("/users/followed")
    public List<SimpleGameDTO> userFollowed() {
        return gameDtoOperations.userFollowed();
    }

    @GetMapping("/users/profile")
    public UserProfileDTO getUserData() {
        return userDtoOperations.getUserData();
    }

    @PutMapping("/users/password")
    public void changePassword(@RequestBody NewPasswordDTO newPasswordDTO) {
        userDtoOperations.changePassword(newPasswordDTO);
    }

    @PutMapping("/users/email")
    public void changeEmail(@RequestBody NewEmailDTO newEmailDTO) {
        userDtoOperations.changeEmail(newEmailDTO);
    }

    @GetMapping("/users/checker/login")
    public Boolean loginExists(@RequestParam String login) {
        return userDtoOperations.isLoginAvailable(login);
    }

    @GetMapping("/users/checker/email")
    public Boolean emailExists(@RequestParam String email) {
        return userDtoOperations.isEmailAvailable(email);
    }
}




