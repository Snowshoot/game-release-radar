package com.Radar.Service.DtoOperations;

import com.Radar.Entity.GameEntity;
import com.Radar.Entity.PublisherEntity;
import com.Radar.Rest.DTO.DetailedDTO.PublisherDTO;
import com.Radar.Rest.DTO.SimpleDTO.GamesWithCountDTO;
import com.Radar.Rest.DTO.SimpleDTO.SimpleGameDTO;
import com.Radar.Rest.DTO.SimpleDTO.SimplePublisherDTO;
import com.Radar.Service.DbService.GameService;
import com.Radar.Service.DbService.PublisherService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class PublisherDtoOperations {
    @Autowired
    private GameDtoOperations game2DTO;
    @Autowired
    private PublisherService publisherService;
    @Autowired
    private GameService gameService;

    public List<SimplePublisherDTO> publishersToNamesList(){
        List<PublisherEntity> publishers = publisherService.findAll();
        return publishers.stream().distinct().map(publisher -> new SimplePublisherDTO(publisher.getPublisherId(), publisher.getName())).collect(Collectors.toList());
    }

    public List<SimplePublisherDTO> gamePublisherSetToNames(Set<PublisherEntity> publishers){
        return publishers.stream().distinct().map(publisher -> new SimplePublisherDTO(publisher.getPublisherId(), publisher.getName())).collect(Collectors.toList());
    }
    public PublisherDTO publisherToDetails(Integer publisherId, Integer page, Integer pageSize){
        Optional<PublisherEntity> publisher = publisherService.findById(publisherId);
        ImmutablePair<List<GameEntity>, Long> simpleGames = gameService.gamesPagedByPub(publisherId, page, pageSize);
        List<SimpleGameDTO> basicGames = game2DTO.gamesToBasicDataGames(simpleGames.left);
        GamesWithCountDTO countDTO = new GamesWithCountDTO(simpleGames.right, basicGames);
        return new PublisherDTO(publisher.get().getPublisherId(), publisher.get().getName(), countDTO);
    }
}
