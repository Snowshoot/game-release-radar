package com.Radar.Repository;

import com.Radar.Entity.StoreUrlEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StoreUrlRepository extends JpaRepository<StoreUrlEntity, Integer>{
    @Query("SELECT g.stores from GameEntity g where g.gameApiId = :gameApiId")
    List<StoreUrlEntity> findByGameId(@Param("gameApiId") Integer gameApiId);

    @Query("SELECT s from StoreUrlEntity s where s.storeUrlApiId = :apiId")
    StoreUrlEntity findByApiId(@Param("apiId") Integer apiId);
}
