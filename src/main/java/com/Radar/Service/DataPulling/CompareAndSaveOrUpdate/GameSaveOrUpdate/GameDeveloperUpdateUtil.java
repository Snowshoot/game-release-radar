package com.Radar.Service.DataPulling.CompareAndSaveOrUpdate.GameSaveOrUpdate;

import com.Radar.Entity.DeveloperEntity;
import com.Radar.Entity.GenreEntity;
import com.Radar.Feign.Response.DeveloperResponse;
import com.Radar.Service.DbService.DeveloperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
@Service
public class GameDeveloperUpdateUtil {
    @Autowired
    private DeveloperService developerService;

    public Set<DeveloperEntity> developerResponseToEntities (Set<DeveloperResponse> developerResponse){
        return developerResponse.stream()
                .peek(this::developerSaveIfNotExists)
                .map(developer -> developerService.findByApiId(developer.getId())).collect(Collectors.toSet());
    }

    private void developerSaveIfNotExists(DeveloperResponse developer) {
        DeveloperEntity tempDev = developerService.findByApiId(developer.getId());
        if (tempDev == null) {
            DeveloperEntity newDev = new DeveloperEntity(developer.getName(), developer.getId());
            developerService.save(newDev);
        }
    }

    public Boolean areNotSame(Set<DeveloperResponse> newGameDevelopers, Set<DeveloperEntity> baseGameDevelopers){
        return !gameResponseToGameDeveloperIds(newGameDevelopers).equals(getGameDeveloperIds(baseGameDevelopers));
    }

    public List<Integer> getGameDeveloperIds(Set<DeveloperEntity> oldDevs) {
        return oldDevs.stream().map(DeveloperEntity::getDeveloperApiId).sorted().collect(Collectors.toList());
    }

    private List<Integer> gameResponseToGameDeveloperIds(Set<DeveloperResponse> developerResponse) {
        return developerResponse.stream().map(DeveloperResponse::getId).sorted().collect(Collectors.toList());
    }
}

