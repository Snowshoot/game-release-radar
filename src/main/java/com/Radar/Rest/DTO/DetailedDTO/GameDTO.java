package com.Radar.Rest.DTO.DetailedDTO;

import com.Radar.Rest.DTO.SimpleDTO.SimpleDeveloperDTO;
import com.Radar.Rest.DTO.SimpleDTO.SimpleGenreDTO;
import com.Radar.Rest.DTO.SimpleDTO.SimplePublisherDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class GameDTO {
    private Integer id;
    private String name;
    private String cover;
    private String released;
    private String description;
    private Double averageRating;
    private Boolean tba;
    private List<SimpleGenreDTO> genres;
    private List<StoreUrlDTO> stores;
    private List<SimpleDeveloperDTO> developers;
    private List<SimplePublisherDTO> publishers;
}
