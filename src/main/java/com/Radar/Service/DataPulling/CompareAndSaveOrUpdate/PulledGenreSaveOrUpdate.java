package com.Radar.Service.DataPulling.CompareAndSaveOrUpdate;

import com.Radar.Entity.GenreEntity;
import com.Radar.Feign.Response.GenreResponse;
import com.Radar.Service.DbService.GenreService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PulledGenreSaveOrUpdate {
    @Autowired
    private GenreService genreService;

    public void compareGenreAndSaveOrUpdate(GenreResponse newGen) {
        GenreEntity baseGenre = genreService.findByApiId(newGen.getId());
        if(baseGenre != null) {
            GenreResponse oldGen = transformGenreEntityToResponse(baseGenre);
            if (!newGen.equals(oldGen)) {
                genreService.update(newGen);
            }
            else {
                log.info("Genre was not updated");
            }
        }
        else{
            genreService.save(new GenreEntity(newGen.getName(), newGen.getId()));
        }
    }

    private GenreResponse transformGenreEntityToResponse(GenreEntity baseGenre) {
        return new GenreResponse(baseGenre.getGenreApiId(), baseGenre.getGenre());
    }
}
