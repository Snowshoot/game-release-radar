package com.Radar.Entity;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@Table(name = "publishers", uniqueConstraints = {
        @UniqueConstraint(columnNames = "name")
})
public class PublisherEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "publisher_id", unique = true, nullable = false)
    private Integer publisherId;

    @Column(name = "name", unique = true, nullable = false)
    @NonNull private String name;

    @Column(name = "pub_api_id", unique = true, nullable = false)
    @NonNull private Integer publisherApiId;

    @ManyToMany(mappedBy = "publishers", cascade = CascadeType.ALL)
    private Set<GameEntity> publisherGames = new HashSet<>();
}
