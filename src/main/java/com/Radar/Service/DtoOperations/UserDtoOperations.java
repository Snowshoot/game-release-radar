package com.Radar.Service.DtoOperations;

import com.Radar.Entity.GameEntity;
import com.Radar.Entity.RoleEntity;
import com.Radar.Entity.UserEntity;
import com.Radar.Rest.DTO.DetailedDTO.UserDTO;
import com.Radar.Rest.DTO.SimpleDTO.NewEmailDTO;
import com.Radar.Rest.DTO.SimpleDTO.NewPasswordDTO;
import com.Radar.Rest.DTO.SimpleDTO.UserProfileDTO;
import com.Radar.Security.Utils.JWTUtil;
import com.Radar.Service.DbService.FollowedService;
import com.Radar.Service.DbService.GameService;
import com.Radar.Service.DbService.RoleService;
import com.Radar.Service.DbService.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
public class UserDtoOperations {
    @Autowired
    private UserService userService;
    @Autowired
    private GameService gameService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private FollowedService followedService;

    public String getCurrentUser(){
        User currentUser = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return currentUser.getUsername();
    }

    public Boolean isLoginAvailable(String login){
        return userService.isLoginAvailable(login);
    }

    public Boolean isEmailAvailable(String email){
        return userService.isEmailAvailable(email);
    }

    public void saveNewUser(UserDTO user){
        Optional<RoleEntity> role = roleService.findById(1);
        Set<RoleEntity> roles = new HashSet<>();
        roles.add(role.get());
        UserEntity newUser = new UserEntity(user.getLogin(), user.getPassword(), user.getEmail(), roles);
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        newUser.setPassword(encoder.encode(newUser.getPassword()));
        userService.saveNewUser(newUser);
    }

    public UserProfileDTO getUserData(){
        return userEntityToDto(userService.findByLogin(getCurrentUser()));
    }

    public void changePassword(NewPasswordDTO passwordDTO){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        UserEntity currentUser = userService.findByLogin(getCurrentUser());
        if(encoder.matches(passwordDTO.getOldPassword(), currentUser.getPassword())) {
            userService.changePassword(currentUser, encoder.encode(passwordDTO.getNewPassword()));
            }
        }

    public void changeEmail(NewEmailDTO emailDTO){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        UserEntity currentUser = userService.findByLogin(getCurrentUser());
        if(encoder.matches(emailDTO.getOldPassword(), currentUser.getPassword())) {
            userService.changeEmail(currentUser, emailDTO.getNewEmail());
        }
    }

    private UserProfileDTO userEntityToDto(UserEntity userEntity){
        return new UserProfileDTO(userEntity.getLogin(), userEntity.getEmail());
    }

    @Transactional
    public void addFollowed(String userName, Integer gameId){
        UserEntity currentUser = userService.findByLogin(userName);
        Optional<GameEntity> game = gameService.findById(gameId);
        followedService.addFollowed(currentUser, game.get());
    }

    @Transactional
    public void removeFollowed(String userName, Integer gameId){
        Optional<GameEntity> game = gameService.findById(gameId);
        followedService.removeFollowed(userName, game.get());
    }

    @Transactional
    public void addRole(String userName, Integer roleId){
        UserEntity user = userService.findByLogin(userName);
        Optional<RoleEntity> role = roleService.findById(roleId);
        userService.addRole(user, role.get());
    }

    @Transactional
    public void removeRole(String userName, Integer roleId){
        UserEntity user = userService.findByLogin(userName);
        Optional<RoleEntity> role = roleService.findById(roleId);
        userService.removeRole(user, role.get());
    }
}
