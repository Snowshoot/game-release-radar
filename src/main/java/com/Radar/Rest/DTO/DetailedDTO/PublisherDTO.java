package com.Radar.Rest.DTO.DetailedDTO;

import com.Radar.Rest.DTO.SimpleDTO.GamesWithCountDTO;
import com.Radar.Rest.DTO.SimpleDTO.SimpleGameDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class PublisherDTO {
    private Integer id;
    private String name;
    private GamesWithCountDTO basicGames;
}
