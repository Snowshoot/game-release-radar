package com.Radar.Rest.DTO.SimpleDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class UserPageReviewDTO {
    private Integer gameId;
    private String gameCover;
    private Integer stars;
    private String review;
    private String game;
}
