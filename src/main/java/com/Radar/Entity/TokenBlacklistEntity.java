package com.Radar.Entity;

import com.Radar.Entity.Enum.RefreshOrAuthTokenEnum;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@Table(name = "token_blacklist", uniqueConstraints = {
        @UniqueConstraint(columnNames = "token_id")})
public class TokenBlacklistEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "token_id", unique = true, nullable = false)
    private Integer tokenId;

    @Column(name = "token", unique = true, nullable = false)
    @NonNull private String token;

    @Enumerated(EnumType.STRING)
    @Column(name = "token_type", nullable = false)
    @NonNull private RefreshOrAuthTokenEnum refreshOrAuthTokenEnum;

    @Column(name = "valid_until", nullable = false)
    @NonNull private Date validUntil;
}
