package com.Radar.Service.DbService;

import com.Radar.Entity.StoreEntity;
import com.Radar.Entity.StoreUrlEntity;
import com.Radar.Repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StoreService {
    @Autowired
    private StoreRepository storeRepository;

    public List<StoreEntity> findAll(){return storeRepository.findAll();}

    public Optional<StoreEntity> findById(Integer storeId){return storeRepository.findById(storeId);}

    public List<StoreEntity> findByGameId(Integer apiId){
        return storeRepository.findByGameId(apiId);
    }

    public StoreEntity findByApiId(Integer apiId){
        return storeRepository.findByApiId(apiId);
    }

    public void save(StoreEntity store) {storeRepository.save(store);}
}
