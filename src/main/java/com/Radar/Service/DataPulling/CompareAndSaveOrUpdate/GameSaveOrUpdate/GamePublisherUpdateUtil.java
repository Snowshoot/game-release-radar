package com.Radar.Service.DataPulling.CompareAndSaveOrUpdate.GameSaveOrUpdate;

import com.Radar.Entity.PublisherEntity;
import com.Radar.Entity.StoreUrlEntity;
import com.Radar.Feign.Response.PublisherResponse;
import com.Radar.Service.DbService.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class GamePublisherUpdateUtil {
    @Autowired
    private PublisherService publisherService;

    public Set<PublisherEntity> publisherResponseToEntities(Set<PublisherResponse> publisherResponse){
        return publisherResponse.stream()
                .peek(this::publisherSaveIfNotExists)
                .map(publisher -> publisherService.findByApiId(publisher.getId())).collect(Collectors.toSet());
    }

    private void publisherSaveIfNotExists(PublisherResponse publisher) {
        PublisherEntity tempPub = publisherService.findByApiId(publisher.getId());
        if (tempPub == null) {
            PublisherEntity newPub = new PublisherEntity(publisher.getName(), publisher.getId());
            publisherService.save(newPub);
        }
    }

    public Boolean areNotSame(Set<PublisherResponse> newGamePublishers, Set<PublisherEntity> baseGamePublishers){
        return !gameResponseToGamePublisherIds(newGamePublishers).equals(getGamePublisherIds(baseGamePublishers));
    }

    public List<Integer> getGamePublisherIds(Set<PublisherEntity> oldPublishers) {
        return oldPublishers.stream().map(PublisherEntity::getPublisherApiId).sorted().collect(Collectors.toList());
    }

    private List<Integer> gameResponseToGamePublisherIds(Set<PublisherResponse> publisherResponse) {
        return publisherResponse.stream().map(PublisherResponse::getId).sorted().collect(Collectors.toList());
    }
}
