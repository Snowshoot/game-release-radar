package com.Radar.Repository;

import com.Radar.Entity.GameEntity;
import com.Radar.Entity.ReviewEntity;
import com.Radar.Entity.StoreEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ReviewRepository extends JpaRepository<ReviewEntity, Integer>, JpaSpecificationExecutor<ReviewEntity> {
    @Query("SELECT u.reviews from UserEntity u where u.userId = :userId")
    List<ReviewEntity> findByUserPaged(@Param("userId") Integer userId, Pageable pageable);

    @Query("SELECT g.reviews from GameEntity g where g.gameId = :gameId")
    List<ReviewEntity> findByGamePaged(@Param("gameId") Integer gameId, Pageable pageable);

    @Query("SELECT g.reviews from GameEntity g where g.gameId = :gameId")
    List<ReviewEntity> findByGame(@Param("gameId") Integer gameId);

    @Transactional
    @Modifying
    @Query("Delete from ReviewEntity r where r.userId.userId = :userId AND r.gameId.gameId = :gameId")
    Integer deleteUsersGameReview(@Param("userId") Integer userId, @Param("gameId") Integer gameId);
}
