package com.Radar.Rest.Controllers;

import com.Radar.Rest.DTO.SimpleDTO.SimpleGenreDTO;
import com.Radar.Service.DtoOperations.GenreDtoOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GenreRestController {
    @Autowired
    private GenreDtoOperations genreDTOOperations;

    @GetMapping("/genres")
    List<SimpleGenreDTO> findAll(){
        return genreDTOOperations.genresToNameList();
    }
}
