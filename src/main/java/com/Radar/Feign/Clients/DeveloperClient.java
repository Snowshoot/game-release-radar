package com.Radar.Feign.Clients;

import com.Radar.Feign.Response.DeveloperResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "receivedDevelopers", url = "localhost:8290")
public interface DeveloperClient {
    @RequestMapping(method = RequestMethod.GET, value = "/developers")
    List<DeveloperResponse> getDevelopers(@RequestParam Integer page, @RequestParam(name = "page_size") Integer pageSize);
}