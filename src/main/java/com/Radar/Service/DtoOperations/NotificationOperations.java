package com.Radar.Service.DtoOperations;

import com.Radar.Entity.GameEntity;
import com.Radar.Rest.DTO.SimpleDTO.GamesWithCountDTO;
import com.Radar.Rest.DTO.SimpleDTO.SimpleGameDTO;
import com.Radar.Service.DbService.FollowedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class NotificationOperations {
    @Autowired
    UserDtoOperations userDtoOperations;
    @Autowired
    FollowedService followedService;
    @Autowired
    GameDtoOperations gameDtoOperations;

    public GamesWithCountDTO countedNotifications(){
        String userName = userDtoOperations.getCurrentUser();
        List<SimpleGameDTO> releasingGames = gameDtoOperations.gamesToBasicDataGames(followedService.findUserNotificationGames(userName));
        return new GamesWithCountDTO((long) releasingGames.size(), releasingGames);
    }

    public void removeNotifications(){
        String userName = userDtoOperations.getCurrentUser();
        followedService.disableNotifications(userName);
    }
}
