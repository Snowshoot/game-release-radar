package com.Radar.Rest.DTO.SimpleDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class UserReviewsWithCount {
    private Long count;
    private List<UserPageReviewDTO> reviews;
}
