package com.Radar.Service.DbService;

import com.Radar.Entity.ReviewEntity;
import com.Radar.Repository.ReviewRepository;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReviewService {
    @Autowired
    private ReviewRepository reviewRepository;

    public void save(ReviewEntity review){reviewRepository.save(review);}

    public ImmutablePair<List<ReviewEntity>, Long> reviewsByUserPaged(Integer userId, Integer page, Integer pageSize){
        Pageable pageable = PageRequest.of(page - 1, pageSize);
        Specification<ReviewEntity> spec = Specification.where(byUser(userId));

        return ImmutablePair.of(
                reviewRepository.findAll(spec, pageable).stream().collect(Collectors.toList()),
                reviewRepository.count(spec));
    }

    public ImmutablePair<List<ReviewEntity>, Long> reviewsByGamePaged(Integer gameId, Integer page, Integer pageSize){
        Pageable pageable = PageRequest.of(page - 1, pageSize);
        Specification<ReviewEntity> spec = Specification.where(byGame(gameId));
        reviewRepository.count(spec);
        return ImmutablePair.of(reviewRepository.findAll(spec, pageable).stream().collect(Collectors.toList()), reviewRepository.count(spec));
    }

    public List<ReviewEntity> reviewsByGame(Integer gameId){
        return reviewRepository.findByGame(gameId);
    }

    public void delete(Integer userId, Integer gameId){reviewRepository.deleteUsersGameReview(userId, gameId);}

    static Specification<ReviewEntity> byGame(Integer gameId) {
        return (review, cq, cb) -> {
            cq.distinct(true);
            return cb.equal(review.join("gameId").get("gameId"), gameId);
        };
    }

    static Specification<ReviewEntity> byUser(Integer userId) {
        return (review, cq, cb) -> {
            cq.distinct(true);
            return cb.equal(review.join("userId").get("userId"), userId);
        };
    }
}
