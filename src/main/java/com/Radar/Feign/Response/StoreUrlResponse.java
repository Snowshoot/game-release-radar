package com.Radar.Feign.Response;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class StoreUrlResponse {
    private Integer storeUrlApiId;
    private String url;
    private Integer storeApiId;
    private String storeName;
}
