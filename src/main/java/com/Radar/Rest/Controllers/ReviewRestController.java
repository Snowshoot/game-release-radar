package com.Radar.Rest.Controllers;

import com.Radar.Rest.DTO.DetailedDTO.ReviewDTO;
import com.Radar.Rest.DTO.SimpleDTO.GameReviewDTO;
import com.Radar.Rest.DTO.SimpleDTO.UserPageReviewDTO;
import com.Radar.Service.DtoOperations.ReviewDtoOperations;
import com.Radar.Service.DtoOperations.UserDtoOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ReviewRestController {
    @Autowired
    private ReviewDtoOperations reviewDtoOperations;

    @PostMapping("/reviews")
    public void addReview(@RequestBody ReviewDTO review){
        reviewDtoOperations.saveNewReview(review);
    }

    @DeleteMapping("/reviews")
    public void deleteReview(@RequestParam(name = "gameId") Integer gameId){
        reviewDtoOperations.removeReview(gameId);
    }
}
