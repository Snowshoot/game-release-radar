package com.Radar.Service.DtoOperations;

import com.Radar.Entity.DeveloperEntity;
import com.Radar.Entity.GameEntity;
import com.Radar.Rest.DTO.DetailedDTO.DeveloperDTO;
import com.Radar.Rest.DTO.SimpleDTO.GamesWithCountDTO;
import com.Radar.Rest.DTO.SimpleDTO.SimpleDeveloperDTO;
import com.Radar.Rest.DTO.SimpleDTO.SimpleGameDTO;
import com.Radar.Service.DbService.DeveloperService;
import com.Radar.Service.DbService.GameService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DeveloperDtoOperations {
    @Autowired
    private GameDtoOperations game2DTO;
    @Autowired
    private DeveloperService developerService;
    @Autowired
    private GameService gameService;

    public List<SimpleDeveloperDTO> developersToNamesList(){
        List<DeveloperEntity> developers = developerService.findAll();
        return developers.stream().distinct().map(developer -> new SimpleDeveloperDTO(developer.getDeveloperId(), developer.getName())).collect(Collectors.toList());
    }

    public List<SimpleDeveloperDTO> gameDeveloperSetToNames(Set<DeveloperEntity> developers){
        return developers.stream().distinct().map(developer -> new SimpleDeveloperDTO(developer.getDeveloperId(), developer.getName())).collect(Collectors.toList());
    }

    public DeveloperDTO developerToDeveloperDetails(Integer developerId, Integer page, Integer pageSize){
        Optional<DeveloperEntity> developer = developerService.findById(developerId);
        ImmutablePair<List<GameEntity>, Long> devGames = gameService.gamesPagedByDev(developerId, page, pageSize);
        List<SimpleGameDTO> basicGames = game2DTO.gamesToBasicDataGames(devGames.left);
        GamesWithCountDTO countDTO = new GamesWithCountDTO(devGames.right, basicGames);
        return new DeveloperDTO(developer.get().getDeveloperId(), developer.get().getName(), countDTO);
    }
}
