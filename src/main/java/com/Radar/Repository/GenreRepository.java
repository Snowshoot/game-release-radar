package com.Radar.Repository;

import com.Radar.Entity.GenreEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GenreRepository extends JpaRepository<GenreEntity, Integer> {
    @Query("SELECT g.genres from GameEntity g where g.gameApiId = :gameApiId")
    List<GenreEntity> findByGameId(@Param("gameApiId") Integer gameApiId);

    @Query("SELECT g from GenreEntity g where g.genreApiId = :apiId")
    GenreEntity findByApiId(@Param("apiId") Integer apiId);
}
