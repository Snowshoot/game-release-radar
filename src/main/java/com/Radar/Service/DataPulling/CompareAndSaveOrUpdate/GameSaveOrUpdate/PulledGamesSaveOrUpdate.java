package com.Radar.Service.DataPulling.CompareAndSaveOrUpdate.GameSaveOrUpdate;

import com.Radar.Entity.*;
import com.Radar.Feign.Response.*;

import com.Radar.Service.DbService.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@Service
@Slf4j
public class PulledGamesSaveOrUpdate {
    @Autowired
    private GamePublisherUpdateUtil gamePublisherUpdateUtil;
    @Autowired
    private GameDeveloperUpdateUtil gameDeveloperUpdateUtil;
    @Autowired
    private GameGenreUpdateUtil gameGenreUpdateUtil;
    @Autowired
    private GameStoreUrlUpdateUtil gameStoreUrlUpdateUtil;
    @Autowired
    private GameService gameService;
    @Autowired
    private StoreUrlService storeUrlService;

    @Transactional
    public void compareGameAndSaveOrUpdate(GameResponse newGame) {
        GameEntity baseGame = gameService.findByApiId(newGame.getId());
        Set<GenreEntity> gameGenres = gameGenreUpdateUtil.GenreResponseToEntities(newGame.getGenres());
        Set<DeveloperEntity> gameDevs = gameDeveloperUpdateUtil.developerResponseToEntities(newGame.getDevelopers());
        Set<PublisherEntity> gamePubs = gamePublisherUpdateUtil.publisherResponseToEntities(newGame.getPublishers());
        if (baseGame != null) {
            GameResponse oldGame = transformGameEntityToResponse(baseGame);
            if (!newGame.equals(oldGame) ||
                    gameGenreUpdateUtil.areNotSame(newGame.getGenres(), baseGame.getGenres()) ||
                    gameStoreUrlUpdateUtil.areNotSame(newGame.getStores(), baseGame.getStores()) ||
                    gamePublisherUpdateUtil.areNotSame(newGame.getPublishers(), baseGame.getPublishers()) ||
                    gameDeveloperUpdateUtil.areNotSame(newGame.getDevelopers(), baseGame.getDevelopers())) {
                Set<StoreUrlEntity> gameStoreUrls = gameStoreUrlUpdateUtil.storeUrlResponseToEntities(newGame.getStores(), newGame.getId());
                gameService.update(newGame, gameGenres, gameDevs, gamePubs, gameStoreUrls);
            } else {
                log.info("game was not updated");
            }
        } else {
            GameEntity tempGame = new GameEntity(newGame.getName(), newGame.getCover(), newGame.getReleased(), newGame.getDescription(), newGame.getId(), newGame.getTba(), gameGenres, gameDevs, gamePubs);
            gameService.save(tempGame);
            gameStoreUrlUpdateUtil.storeUrlResponseToEntities(newGame.getStores(), newGame.getId());
        }
    }

    public GameResponse transformGameEntityToResponse(GameEntity baseGame) {
        return new GameResponse(baseGame.getGameApiId(), baseGame.getTitle(), baseGame.getCover(), baseGame.getRelease(), baseGame.getDescription(), baseGame.getTba());
    }
}