package com.Radar.Service.ScheduledDatabaseTasks;

import com.Radar.Entity.TokenBlacklistEntity;
import com.Radar.Service.DbService.TokenBlacklistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class TokenBlacklistScheduledClearing {
    @Autowired
    private TokenBlacklistService tokenBlacklistService;

    @Scheduled(fixedDelay = 86400000)
    public void clearExpiredTokens(){
        List<TokenBlacklistEntity> blacklistedTokens = tokenBlacklistService.findAll();
        if(!blacklistedTokens.isEmpty()) {
            blacklistedTokens.stream().filter(token -> token.getValidUntil().before(new Date())).forEach(this::deleteExpired);
        }
    }

    private void deleteExpired(TokenBlacklistEntity token){
        tokenBlacklistService.delete(token);
    }
}