package com.Radar.Repository;

import com.Radar.Entity.StoreUrlEntity;
import com.Radar.Entity.TokenBlacklistEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenBlacklistRepository extends JpaRepository<TokenBlacklistEntity, Integer> {
    @Query("SELECT t from TokenBlacklistEntity t where t.token = :token")
    TokenBlacklistEntity findByToken(@Param("token") String token);
}
