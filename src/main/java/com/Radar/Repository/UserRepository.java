package com.Radar.Repository;

import com.Radar.Entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {
    @Query("SELECT u from UserEntity u where u.login = :login")
    UserEntity findByLogin(@Param("login") String login);

    @Query("Select u from UserEntity u where u.email = :email")
    UserEntity findByEmail(@Param("email") String email);
}
