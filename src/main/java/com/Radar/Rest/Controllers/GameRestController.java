package com.Radar.Rest.Controllers;

import com.Radar.Entity.GameEntity;
import com.Radar.Rest.DTO.DetailedDTO.GameDTO;
import com.Radar.Rest.DTO.DetailedDTO.UserDTO;
import com.Radar.Rest.DTO.SimpleDTO.GameReviewDTO;
import com.Radar.Rest.DTO.SimpleDTO.GameReviewsWithCount;
import com.Radar.Rest.DTO.SimpleDTO.GamesWithCountDTO;
import com.Radar.Rest.DTO.SimpleDTO.SimpleGameDTO;
import com.Radar.Service.DbService.FollowedService;
import com.Radar.Service.DbService.GameService;
import com.Radar.Service.DtoOperations.GameDtoOperations;
import com.Radar.Service.DtoOperations.ReviewDtoOperations;
import com.Radar.Service.DtoOperations.UserDtoOperations;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
public class GameRestController {
    @Autowired
    private GameDtoOperations gameDtoOperations;
    @Autowired
    private UserDtoOperations userDtoOperations;
    @Autowired
    private GameService gameService;
    @Autowired
    private ReviewDtoOperations reviewDtoOperations;
    @Autowired
    private FollowedService followedService;

    @GetMapping("/games")
    public GamesWithCountDTO findGamesPaged(
            @RequestParam(value = "query", required = false) String query,
            @RequestParam(value = "genres", required = false) String genres,
            @RequestParam(value = "stores", required = false) String stores,
            @RequestParam(value = "noStores", required = false) Boolean noStores,
            @RequestParam(value = "tba", required = false) Boolean tba,
            @RequestParam(value = "excludeReleased", required = false) Boolean excludeReleased,
            @RequestParam(name = "page") Integer page,
            @RequestParam(name = "pageSize") Integer pageSize){
        return gameDtoOperations.searchGamesPaged(page, pageSize, query, genres, stores, noStores, tba, excludeReleased);
    }

//    @GetMapping("/notifications")
//    public List<SimpleGameDTO> getGameNotifications(){
//        String currentUser = userDtoOperations.getCurrentUser();
//
//    }

    @GetMapping("/games/popular")
    public List<SimpleGameDTO> findPopular(@RequestParam(name = "page") Integer page, @RequestParam(name = "pageSize") Integer pageSize){
        return gameDtoOperations.gamesToBasicDataGames(gameService.mostFollowed(page, pageSize));
    }

    @GetMapping("/games/rating")
    public List<SimpleGameDTO> findTopRated(@RequestParam(name = "page") Integer page, @RequestParam(name = "pageSize") Integer pageSize){
        return gameDtoOperations.gamesToBasicDataGames(gameService.findTopRated(page, pageSize));
    }

    @GetMapping("/games/soon")
    public List<SimpleGameDTO> findReleasingSoon(@RequestParam(name = "page") Integer page, @RequestParam(name = "pageSize") Integer pageSize){
        return gameDtoOperations.gamesToBasicDataGames(gameService.findReleasingSoon(page, pageSize));
    }

    @GetMapping("/games/{id}")
    public GameDTO findById(@PathVariable(name = "id") Integer gameId){
        return gameDtoOperations.gameToGameDetails(gameId);
    }

    @GetMapping("/games/{gameId}/reviews")
    public GameReviewsWithCount findByGame(@PathVariable Integer gameId, @RequestParam(name = "page") Integer page, @RequestParam(name = "pageSize") Integer pageSize){
        return reviewDtoOperations.reviewsToDTOByGame(gameId,page, pageSize);
    }

    @PutMapping("/games/followed")
    public void followedGame(@RequestParam(name = "gameId") Integer gameId){
        String currentUser = userDtoOperations.getCurrentUser();
        userDtoOperations.addFollowed(currentUser, gameId);
    }

    @DeleteMapping("/games/followed")
    public void removeFollowedGame(@RequestParam(name = "gameId") Integer gameId){
        String currentUser = userDtoOperations.getCurrentUser();
        userDtoOperations.removeFollowed(currentUser, gameId);
    }
}
