package com.Radar.Entity;

import lombok.*;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@Table(name = "reviews")
public class ReviewEntity {

    @EmbeddedId
    private ReviewEntityPrimaryKey reviewId;

    @ManyToOne
    @MapsId("gameReviewId")
    @JoinColumn(name = "game_id")
    private GameEntity gameId;

    @ManyToOne
    @MapsId("userReviewId")
    @JoinColumn(name = "user_id")
    private UserEntity userId;

    @Column(name = "stars", nullable = false)
    @NonNull private Integer stars;

    @Column(name = "review", nullable = false, length = 1000)
    @NonNull private String review;
}
