package com.Radar.Entity;

import com.Radar.Entity.Enum.RoleEnum;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@Table(name = "roles", uniqueConstraints = {
        @UniqueConstraint(columnNames = "role_id")})
public class RoleEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "role_id", unique = true, nullable = false)
    private Integer roleId;

    @Column(name = "name", unique = true, nullable = false)
    @Enumerated(EnumType.STRING)
    @NonNull private RoleEnum role;

    @ManyToMany(mappedBy = "roles")
    private Set<UserEntity> userRoles = new HashSet<>();
}
