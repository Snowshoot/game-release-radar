package com.Radar;

import com.Radar.Entity.Enum.RoleEnum;
import com.Radar.Entity.RoleEntity;
import com.Radar.Entity.StoreEntity;
import com.Radar.Service.DbService.RoleService;
import com.Radar.Service.DbService.StoreService;
import com.Radar.Service.DbService.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Slf4j
public class PostConstructAddData {
    @Autowired
    private StoreService storeService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private UserService userService;
    @PostConstruct
    void populateDatabase() { //TODO add check if exist before so can run smoothly, maybe create order so it works anytime
//        StoreEntity steam = new StoreEntity("Steam", 1, "https://upload.wikimedia.org/wikipedia/commons/thumb/8/83/Steam_icon_logo.svg/1200px-Steam_icon_logo.svg.png");
//        StoreEntity gog = new StoreEntity("GOG", 5, "http://polloita.altervista.org/wp-content/uploads/2015/04/GOG-Logo.jpg");
//        StoreEntity itch = new StoreEntity("itch.io", 9, "http://kentuckyroutezero.com/images/itchio-logo.png");
//        StoreEntity epic = new StoreEntity("Epic Games", 11, "https://cdn2.unrealengine.com/egcom/community/wp-content/uploads/2012/12/epic_store_logo1.jpg");
//        RoleEntity user = new RoleEntity(RoleEnum.USER);
//        RoleEntity mod = new RoleEntity(RoleEnum.MOD);
//        RoleEntity admin = new RoleEntity(RoleEnum.ADMIN);
//        roleService.save(user);
//        roleService.save(mod);
//        roleService.save(admin);
//        storeService.save(steam);
//        storeService.save(gog);
//        storeService.save(itch);
//        storeService.save(epic);
    }
}
