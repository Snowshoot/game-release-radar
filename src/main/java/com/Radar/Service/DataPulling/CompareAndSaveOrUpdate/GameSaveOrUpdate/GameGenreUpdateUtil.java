package com.Radar.Service.DataPulling.CompareAndSaveOrUpdate.GameSaveOrUpdate;

import com.Radar.Entity.GenreEntity;
import com.Radar.Entity.PublisherEntity;
import com.Radar.Feign.Response.GenreResponse;
import com.Radar.Service.DbService.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class GameGenreUpdateUtil {
    @Autowired
    private GenreService genreService;

    public Set<GenreEntity> GenreResponseToEntities(Set<GenreResponse> genreResponse){
        return genreResponse.stream()
                .peek(this::genreSaveIfNotExists)
                .map(genre -> genreService.findByApiId(genre.getId()))
                .collect(Collectors.toSet());
    }

    private void genreSaveIfNotExists(GenreResponse genre) {
        GenreEntity tempGen = genreService.findByApiId(genre.getId());
        if (tempGen == null) {
            GenreEntity newGen = new GenreEntity(genre.getName(), genre.getId());
            genreService.save(newGen);
        }
    }

    public Boolean areNotSame(Set<GenreResponse> newGameGenres, Set<GenreEntity> baseGameGenres){
        return !gameResponseToGameGenreIds(newGameGenres).equals(getGameGenreIds(baseGameGenres));
    }

    public List<Integer> getGameGenreIds(Set<GenreEntity> oldGenres) {
        return oldGenres.stream().map(GenreEntity::getGenreApiId).sorted().collect(Collectors.toList());
    }

    private List<Integer> gameResponseToGameGenreIds(Set<GenreResponse> genreResponse) {
        return genreResponse.stream().map(GenreResponse::getId).sorted().collect(Collectors.toList());
    }
}
