package com.Radar.Service.DbService;

import com.Radar.Entity.RoleEntity;
import com.Radar.Repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Optional;
import java.util.Set;

@Service
public class RoleService {
    @Autowired
    private RoleRepository roleRepository;

    public void save(RoleEntity role){
        roleRepository.save(role);
    }

    public Optional<RoleEntity> findById(Integer roleId){
        return roleRepository.findById(roleId);
    }

    public Set<RoleEntity> findByUserName(String userName){return roleRepository.findByUserName(userName);}
}
