package com.Radar.Entity;

import lombok.*;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@Table(name = "followed_users_games", uniqueConstraints = {
        @UniqueConstraint(columnNames = "followed_id")
})
public class FollowedEntity {
    public FollowedEntity(GameEntity gameId, UserEntity userId, Boolean notify){
        this.gameId = gameId;
        this.userId = userId;
        this.notify = notify;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "followed_id", unique = true, nullable = false)
    private Integer followedId;

    @ManyToOne
    @JoinColumn(name = "game_id")
    private GameEntity gameId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity userId;

    @Column(name = "notify")
    @NonNull private Boolean notify;
}
