package com.Radar.Rest.DTO.SimpleDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Getter
public class SimpleGameDTO {
    private Integer id;
    private String name;
    private String cover;
    private String released;
    private Double averageRating;
    private Boolean tba;
}
