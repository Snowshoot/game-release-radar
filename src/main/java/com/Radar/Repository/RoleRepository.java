package com.Radar.Repository;

import com.Radar.Entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Integer> {

    @Query("select u.roles from UserEntity u where u.login = :userName")
    Set<RoleEntity> findByUserName(@Param("userName") String userName);
}