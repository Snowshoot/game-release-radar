package com.Radar.Entity;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@Table(name = "developers", uniqueConstraints = {
        @UniqueConstraint(columnNames = "name")
})
public class DeveloperEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "developer_id", unique = true, nullable = false)
    private Integer developerId;

    @Column(name = "name", unique = true, nullable = false)
    @NonNull private String name;

    @Column(name = "dev_api_id", unique = true, nullable = false)
    @NonNull private Integer developerApiId;

    @ManyToMany(mappedBy = "developers", cascade = CascadeType.ALL)
    private Set<GameEntity> developerGames = new HashSet<>();
}
