package com.Radar.Service.DbService;

import com.Radar.Entity.*;
import com.Radar.Feign.Response.GameResponse;
import com.Radar.Feign.Response.StoreUrlResponse;
import com.Radar.Repository.StoreUrlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class StoreUrlService {
    @Autowired
    private StoreUrlRepository storeUrlRepository;

    public List<StoreUrlEntity> findByGameId(Integer apiId){
        return storeUrlRepository.findByGameId(apiId);
    }

    public StoreUrlEntity findByApiId(Integer apiId){
        return storeUrlRepository.findByApiId(apiId);
    }

    public void save(StoreUrlEntity storeUrlEntity){
        storeUrlRepository.save(storeUrlEntity);
    }

    public void update(StoreUrlResponse storeUrlResponse){
        StoreUrlEntity tempStoreUrl = findByApiId(storeUrlResponse.getStoreUrlApiId());
        tempStoreUrl.setUrl(storeUrlResponse.getUrl());
        storeUrlRepository.save(tempStoreUrl);
    }
}
