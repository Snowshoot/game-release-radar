package com.Radar.Security.Filters;

import com.Radar.Entity.Enum.RefreshOrAuthTokenEnum;
import com.Radar.Entity.TokenBlacklistEntity;
import com.Radar.Security.Utils.JWTUtil;
import com.Radar.Service.DbService.TokenBlacklistService;
import com.Radar.Service.DbService.UserDetailsServiceImpl;
import com.Radar.Service.DtoOperations.TokenOperations;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Slf4j
@Component
public class JWTRequestFilter extends OncePerRequestFilter {
    @Autowired
    private UserDetailsServiceImpl userDetailsService;
    @Autowired
    private JWTUtil jwtUtil;
    @Autowired
    private TokenOperations tokenOperations;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        try {
            String userName = null;
            String jwt;
            String refresh;
            try{
                if ((jwt = getJwt(request)) != null) {
                    userName = jwtUtil.extractUserName(jwt);
                }
                if (userName != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                    UserDetails userDetails = this.userDetailsService.loadUserByUsername(userName);
                    if (jwtUtil.validateToken(jwt, userDetails)) {
                        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                                new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                        usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                    }
                }
            }
            catch(ExpiredJwtException e){
                if((refresh = getRefresh(request)) != null) {
                    userName = jwtUtil.extractUserName(refresh);
                    if (userName != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                        UserDetails userDetails = this.userDetailsService.loadUserByUsername(userName);
                        if (jwtUtil.validateToken(refresh, userDetails)) {
                            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                                    new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                            usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                            tokenOperations.createAccessTokenOnValidRefresh(refresh, response);
                        }
                    }
                }
            }
        }
        catch (Exception e){}
            chain.doFilter(request, response);
    }

    private String getJwt(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if ("accessToken".equals(cookie.getName())) {
                String accessToken = cookie.getValue();
                return accessToken;
            }
        }
        return null;
    }

    private String getRefresh(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if ("refreshToken".equals(cookie.getName())) {
                String refreshToken = cookie.getValue();
                return refreshToken;
            }
        }
        return null;
    }
}

