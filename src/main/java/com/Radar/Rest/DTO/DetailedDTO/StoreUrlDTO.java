package com.Radar.Rest.DTO.DetailedDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class StoreUrlDTO {
    private String url;
    private String storeName;
    private String logo;
}
