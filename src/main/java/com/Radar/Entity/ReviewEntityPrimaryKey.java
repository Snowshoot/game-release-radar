package com.Radar.Entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Embeddable
public class ReviewEntityPrimaryKey implements Serializable {
    @Column(name = "game_id")
    private Integer gameReviewId;

    @Column(name = "user_id")
    private Integer userReviewId;
}
