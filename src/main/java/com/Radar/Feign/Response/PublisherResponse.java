package com.Radar.Feign.Response;

import lombok.*;


@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class PublisherResponse {
    @NonNull private Integer id;
    @NonNull private String name;
}
