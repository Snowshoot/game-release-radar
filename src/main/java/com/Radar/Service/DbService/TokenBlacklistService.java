package com.Radar.Service.DbService;

import com.Radar.Entity.TokenBlacklistEntity;
import com.Radar.Repository.TokenBlacklistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TokenBlacklistService {
    @Autowired
    private TokenBlacklistRepository tokenBlacklistRepository;

    public TokenBlacklistEntity findByToken(String token){
        return tokenBlacklistRepository.findByToken(token);
    }

    public void save(TokenBlacklistEntity token){
        tokenBlacklistRepository.save(token);
    }

    public List<TokenBlacklistEntity> findAll(){
        return tokenBlacklistRepository.findAll();
    }

    public void delete(TokenBlacklistEntity token){
        tokenBlacklistRepository.delete(token);
    }
}
