package com.Radar.Service.DtoOperations;

import com.Radar.Entity.StoreUrlEntity;
import com.Radar.Rest.DTO.DetailedDTO.StoreUrlDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class StoreUrlDtoOperations {

    public List<StoreUrlDTO> gameStoresToUrls(Set<StoreUrlEntity> stores){
        return stores.stream().map(store -> new StoreUrlDTO(store.getUrl(), store.getStoreApiId().getName(), store.getStoreApiId().getLogo())).collect(Collectors.toList());
    }
}
