package com.Radar.Service.DtoOperations;

import com.Radar.Entity.StoreEntity;
import com.Radar.Rest.DTO.DetailedDTO.StoreDTO;
import com.Radar.Rest.DTO.SimpleDTO.SimpleGameDTO;
import com.Radar.Rest.DTO.SimpleDTO.SimpleStoreDTO;
import com.Radar.Service.DbService.GameService;
import com.Radar.Service.DbService.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class StoreDtoOperations {
    @Autowired
    private StoreService storeService;

    public List<SimpleStoreDTO> storesToNameAndLogo(){
        List<StoreEntity> stores = storeService.findAll();
        return stores.stream().map(store -> new SimpleStoreDTO(store.getStoreId(), store.getName(), store.getLogo())).collect(Collectors.toList());
    }
}
