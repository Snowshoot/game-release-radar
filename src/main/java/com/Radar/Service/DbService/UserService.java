package com.Radar.Service.DbService;

import com.Radar.Entity.FollowedEntity;
import com.Radar.Entity.GameEntity;
import com.Radar.Entity.RoleEntity;
import com.Radar.Entity.UserEntity;
import com.Radar.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserService{
    @Autowired
    private UserRepository userRepository;

    public void saveNewUser(UserEntity newUser){
        userRepository.save(newUser);
    }

    public Optional<UserEntity> findById (Integer userId){return userRepository.findById(userId);}

    public void changeEmail(UserEntity user, String newEmail){
        user.setEmail(newEmail);
        userRepository.save(user);
    }

    public Boolean isLoginAvailable(String login){
        return userRepository.findByLogin(login) == null;
    }

    public Boolean isEmailAvailable(String email){
        return userRepository.findByEmail(email) == null;
    }

    public void changePassword(UserEntity user, String newPassword){
        user.setPassword(newPassword);
        userRepository.save(user);
    }

    public void addRole(UserEntity user, RoleEntity role){
        Set<RoleEntity> newRoles = user.getRoles();
        newRoles.add(role);
        user.setRoles(newRoles);
        userRepository.save(user);
    }

    public void removeRole(UserEntity user, RoleEntity role){
        Set<RoleEntity> newRoles = user.getRoles();
        newRoles.remove(role);
        user.setRoles(newRoles);
        userRepository.save(user);
    }

    public UserEntity findByLogin(String userName){return userRepository.findByLogin(userName);}
}
