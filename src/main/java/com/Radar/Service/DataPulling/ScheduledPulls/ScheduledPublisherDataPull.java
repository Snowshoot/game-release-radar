//package com.Radar.Service.DataPulling.ScheduledPulls;
//
//import com.Radar.Feign.Response.PublisherResponse;
//import com.Radar.Service.DataPulling.CompareAndSaveOrUpdate.PulledPublishersSaveOrUpdate;
//import com.Radar.Service.DataPulling.DataPullers.PublisherDataPuller;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import java.util.List;
//
//
//@Component
//public class ScheduledPublisherDataPull {
//    @Autowired
//    private PublisherDataPuller publisherDataPuller;
//    @Autowired
//    private PulledPublishersSaveOrUpdate pulledPublishersSaveOrUpdate;
//
//    @Scheduled(fixedDelay = 86400000)
//    public void publisherPullCheckAndDetermineSaveOrUpdate() {
//        List<PublisherResponse> publishers = publisherDataPuller.getPublishers();
//        publishers.forEach(publisherResponse -> pulledPublishersSaveOrUpdate.comparePublisherAndSaveOrUpdate(publisherResponse));
//    }
//}
